﻿
using Calculator.Common;
using Calculator.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Test.Debug
{
    public static class ExtraMethods
    {
        #region Extra - methods
        public static string CreateRandomValidStringVariable()
        {
            Random rnd = new Random();
            string validString = string.Empty;
            // variables forming
            int varLength = rnd.Next(1, 100);
            char[] validVariable = new char[varLength];
            validVariable[0] = SyntaxConst.Letters[rnd.Next(0, SyntaxConst.Letters.Length)];
            for (int j = 1; j < varLength; j++)
            {
                validVariable[j] = SyntaxConst.LettersAndDigits[rnd.Next(0, SyntaxConst.LettersAndDigits.Length)];
            }
            // Concat to expression
            return new string(validVariable);
        }
        public static string CreateRandomCloseValidStringExpression()
        {
            Random rnd = new Random();
            string validString = string.Empty;
            // variables forming
            int varLength = rnd.Next(1, 100);
            char[] validVariable = new char[varLength];
            validVariable[0] = SyntaxConst.Letters[rnd.Next(0, SyntaxConst.Letters.Length)];
            for (int j = 1; j < varLength; j++)
            {
                validVariable[j] = SyntaxConst.SignsLettersDigitsDotComma[rnd.Next(0, SyntaxConst.SignsLettersDigitsDotComma.Length)];
            }
            // Concat to expression
            return new string(validVariable);
        }
            public static string CreateUniqueStringName(string startName)
        {
            Random rnd = new Random();
            System.Threading.Thread.Sleep(rnd.Next(0, 100));
            return startName + (DateTime.Now.Millisecond * rnd.Next(0, 1000000)).ToString();
        }
            public async static Task<List<string>> ActCalculateAndResolveResults(List<string> expressions, List<string> varNames)
            {
                var calculator = new Calculator.Service.Calculator(new VariableRepository(), new UserRepository());
                var results = new List<string>();
                var validResults = new List<bool>();
                string message = null;
                for (int i = 0; i < expressions.Count; i++)
                {
                    string value = null;
                    var result = await calculator.Calculate(2, "test", expressions[i]);
                    if (calculator.TryGetVariable(2, "test", varNames[i], out value, out message))
                    {
                        results.Add(value);
                    }
                    else
                    {
                        results.Add(null);
                    }
                }
                return results;
            }
        #endregion
    }
}
