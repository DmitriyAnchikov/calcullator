﻿using Calculator.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Test.Debug
{
    public static class ParserTest
    {
        public static void TakeNextVariableTest()
        {
            // arrange
            Random rnd = new Random();
            string validString = string.Empty;
            string invalidString = string.Empty;
            //for (int i = 0; i < x; i++)
            //{
            // variables forming
            int varLength = rnd.Next(1, 100);
            char[] validVariable = new char[varLength];
            char[] invalidVariable = new char[varLength];
            validVariable[0] = SyntaxConst.Letters[rnd.Next(0, SyntaxConst.Letters.Length)];
            invalidVariable[0] = SyntaxConst.SpecificChars[rnd.Next(0, SyntaxConst.SpecificChars.Length)];
            for (int j = 1; j < varLength; j++)
            {
                validVariable[j] = SyntaxConst.LettersAndDigits[rnd.Next(0, SyntaxConst.LettersAndDigits.Length)];
                invalidVariable[j] = SyntaxConst.SpecificChars[rnd.Next(0, SyntaxConst.SpecificChars.Length)];
            }
            // Concat to expression
            validString += new string(validVariable);
            invalidString += new string(invalidVariable);
            // act
            var validResult = Parser.TakeNextVariable(validString, 0);
            var invalidResult = Parser.TakeNextVariable(invalidString, 0);
        }
        public static void TryTakeNextSignTest()
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomValidStringVariable();
            var firstSign = SyntaxConst.AllSigns[rnd.Next(0, SyntaxConst.AllSigns.Count)];
            var firstSignIndex = validString.Length;
            validString += firstSign;
            validString += ExtraMethods.CreateRandomValidStringVariable();
            var secondSign = SyntaxConst.AllSigns[rnd.Next(0, SyntaxConst.AllSigns.Count)];
            var secondSignIndex = validString.Length;
            validString += secondSign;


            // act
            string firstSignResult;
            int firstSignIndexResult;
            string secondSignResult;
            int secondSignIndexResult;
            bool firstResult = Parser.TryTakeNextSign(validString, 0, out firstSignResult, out firstSignIndexResult);
            bool secondResult = Parser.TryTakeNextSign(validString, firstSignIndexResult + firstSignResult.Length, out secondSignResult, out secondSignIndexResult);
        }
        public static void FindFirstBeginingBracketIndexTest()
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomValidStringVariable();
            var firstOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var firstOpenBracketIndex = validString.Length;
            validString += firstOpenBracket;
            var secondVariable = ExtraMethods.CreateRandomValidStringVariable();
            int secondVariableLength = secondVariable.Length;
            validString += secondVariable;
            var secondOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var secondOpenBracketIndex = validString.Length;
            validString += secondOpenBracket;


            // act
            char? firstOpenBracketResult;
            char? secondOpenBracketResult;
            char? invalidSecondOpenBracketResult;
            int firstOpenBracketIndexResult = Parser.FindFirstOpenBracketIndex(validString, out firstOpenBracketResult, 0);
            int secondOpenBracketIndexResult = Parser.FindFirstOpenBracketIndex(validString, out secondOpenBracketResult, firstOpenBracketIndexResult + 1);
            int invalidSecondOpenBracketIndexResult = Parser.FindFirstOpenBracketIndex(validString, out invalidSecondOpenBracketResult, firstOpenBracketIndexResult + 1, secondVariableLength - rnd.Next(0, secondVariableLength - 1));
        }
        public static void FindEndingBracketIndexTest()
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomValidStringVariable(); // adding first valid variable
            var firstOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var firstOpenBracketIndex = validString.Length;
            validString += firstOpenBracket;    // adding first open bracket
            var secondVariable = ExtraMethods.CreateRandomValidStringVariable();
            validString += secondVariable;  // adding second variable
            var secondOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var secondOpenBracketIndex = validString.Length;
            validString += secondOpenBracket;   // adding second open bracket
            var thirdVariable = ExtraMethods.CreateRandomValidStringVariable();
            validString += thirdVariable;  // adding third variable
            var thirdCloseBracket = SyntaxConst.Brackets[secondOpenBracket];
            var thirdCloseBracketIndex = validString.Length;
            validString += thirdCloseBracket;   // adding third close bracket
            var fourthVariable = ExtraMethods.CreateRandomValidStringVariable();
            validString += thirdVariable;  // adding fourth variable
            var fourthCloseBracket = SyntaxConst.Brackets[firstOpenBracket];
            var fourthCloseBracketIndex = validString.Length;
            validString += fourthCloseBracket;   // adding fourth close bracket

            // act
            int fourthCloseBracketIndexResult = Parser.FindClosedBracketIndex(validString, firstOpenBracket, firstOpenBracketIndex);
            int thirdOpenBracketIndexResult = Parser.FindClosedBracketIndex(validString, secondOpenBracket, secondOpenBracketIndex);
            int invalidThirdOpenBracketIndexResult = Parser.FindClosedBracketIndex(validString, SyntaxConst.Brackets.Keys.Except(new char[] { secondOpenBracket }).ToArray()[rnd.Next(0, 2)], secondOpenBracketIndex);
        }
    }
}
