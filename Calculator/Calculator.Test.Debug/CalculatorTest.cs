﻿using Calculator.Common;
using Calculator.Data;
using Calculator.Test.Debug;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Test.Debug
{
    public static class CalculatorTest
    {
        public static async Task CalculateLogicalVariablesTest()
        {
            // arrange
            List<string> validVariableName = new List<string>() { ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A") };
            List<string> validExpressions = new List<string>()
                                                            { 
                                                                validVariableName[0]+" = tRue",
                                                                validVariableName[1]+" = !truE",           
                                                                validVariableName[2]+" = 0.3<=2",
                                                                validVariableName[3]+" = -2.1    /4.2 < 7 && false",
                                                                validVariableName[4]+"=!(88<6) == false",
                                                                validVariableName[5]+" = 13 >=   1",
                                                                validVariableName[6]+" = ((((2*(-7))/14+1)*2) +45)/45 != 1"
                                                            };
            List<bool> expectedResults = new List<bool> { true, false, true, false, false, true, false };
            List<string> invalidVariableName = new List<string>() { ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A") };
            List<string> invalidExpressions = new List<string>()
                                                            { 
                                                                invalidVariableName[0]+" == 12/3",
                                                                invalidVariableName[1]+" = (A – B) * / 4.2 + 23",         // doubling signs  
                                                                invalidVariableName[2]+" = 0<<3",
                                                                invalidVariableName[3]+" = -2    &| 4",
                                                                invalidVariableName[4]+" = (((((2*(-7))/14-1)*2) +45 !!= 1",    // unbalanced brackets
                                                                invalidVariableName[5]+" = 13^5"                          // unknown sign
                                                            };
            // act
            var validVariables = await ExtraMethods.ActCalculateAndResolveResults(validExpressions, validVariableName);


            var invalidVariables = await ExtraMethods.ActCalculateAndResolveResults(invalidExpressions, invalidVariableName);

            //assert
            string test;
            for (int i = 0; i < validVariables.Count; i++)
            {
                test = String.Format("Result[{0}]: {1} = [2}", i, validVariableName[i], validVariables[i]);
            }
            for (int i = 0; i < invalidVariables.Count; i++)
            {
                test = String.Format("Result[{0}]: {1} = {2}", i, invalidVariableName[i], invalidVariables[i]);
            }
        }
        public static async Task CalculateNumericVariablesTest()
        {
            // arrange
            List<string> validVariableName = new List<string>() { ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A") };
            List<string> validExpressions = new List<string>()
                                                            { 
                                                                validVariableName[0]+"= 12/3",
                                                                validVariableName[1]+"= 0,3",
                                                                validVariableName[2]+"= -2.1    /4.2",
                                                                validVariableName[3]+"=((((2*(-7))/14+1)*2) +45  )/    45",
                                                                validVariableName[4]+" = 45/6",
                                                                validVariableName[5]+"= .3",
                                                                validVariableName[6]+" = 12."
                                                            };
            List<double> expectedResults = new List<double> { 4, 3, -0.5, 1, (double)45 / 6, 0.3, 12.0 };
            List<string> invalidVariableName = new List<string>() { ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A"), ExtraMethods.CreateUniqueStringName("A") };
            List<string> invalidExpressions = new List<string>()
                                                            { 
                                                                invalidVariableName[0] + "",                                 // empty epression
                                                                invalidVariableName[1] +" == 12/3",                        // repeat of assignation-char
                                                                invalidVariableName[2] +" = (A – B) * / 4.2 + 23",         // doubling signs  
                                                                invalidVariableName[3] +" = 0.3",                           // absent name of main variable
                                                                invalidVariableName[4] +" = (((((2*(-7))/14-1)*2) +45",    // unbalanced brackets
                                                                invalidVariableName[5] +" = 12/0"                          // division by zero
                                                            };
            // act
            var validVariables = await ExtraMethods.ActCalculateAndResolveResults(validExpressions, validVariableName);


            var invalidVariables = await ExtraMethods.ActCalculateAndResolveResults(invalidExpressions, invalidVariableName);

            //assert
            string test;
            for (int i = 0; i < validVariables.Count; i++)
            {
                test = String.Format("Result[{0}]: {1} = [2}", i, validVariableName[i], validVariables[i]);
                            }
            for (int i = 0; i < invalidVariables.Count; i++)
            {
                test = String.Format("Result[{0}]: {1} = {2}", i, invalidVariableName[i], invalidVariables[i]);
            }
        }
        public static async Task CalculateAwaitedVariablesTest()
        {
            // arrange
            Random rnd = new Random();
            var randomVarNameC = ExtraMethods.CreateUniqueStringName("c");
            var randomVarNameA = ExtraMethods.CreateUniqueStringName("a");
            var randomVarNameB = ExtraMethods.CreateUniqueStringName("b");
            string cExpression = String.Format("{0}={1}*{2}", randomVarNameC, randomVarNameA, randomVarNameB);
            string aExpression = String.Format("{0}={1}", randomVarNameA, 2);
            string bExpression = String.Format("{0}={1}", randomVarNameB, 3);

            // act
            var calculator = new Calculator.Service.Calculator(new VariableRepository(), new UserRepository());
            string message = null;
            var result = await calculator.Calculate(2, "test", cExpression);
            await calculator.Calculate(2, "test", aExpression);
            await calculator.Calculate(2, "test", bExpression);
            string value;
            var totalResult = calculator.TryGetVariable(2, "test", randomVarNameC, out value, out message);

            //assert
            string test;
            test = String.Format("c: {0}", value);
            
        }
    }
}
