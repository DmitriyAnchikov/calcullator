﻿using Calculator.Common;
using System;

namespace Calculator.Test.Debug
{
    public static class ValidatorTest
    {
        public static void BracketControlTest()
        {
            // arrange
            Random rnd = new Random();
            string validString1 = "(()()()(()())())";
            string validString2 = "(()()()(()())())()()(())";
            string invalidString1 = ")((()()()(()())())";
            string invalidString2 = "(()()()(()())())(";
            string invalidString3 = "(()()()(()())()) )(";
            string invalidString4 = "(()()()(()(])())";
            string message;
            // act
            var validResult1 = Validator.BracketControl(validString1, out message);
            var validResult2 = Validator.BracketControl(validString2, out message);
            var invalidResult1 = Validator.BracketControl(invalidString1, out message);
            var invalidResult2 = Validator.BracketControl(invalidString2, out message);
            var invalidResult3 = Validator.BracketControl(invalidString3, out message);
            var invalidResult4 = Validator.BracketControl(invalidString4, out message);
        }
        public static void ActionsControlTest()
        {
            // arrange
            Random rnd = new Random();
            string validString1 = "x +2";
            string validString2 = "2 + 3";
            string validString3 = "1- 2";
            string validString4 = "2 * 8";
            string invalidString1 = "2*/1";
            string invalidString2 = "4,87- 87987";
            string invalidString3 = "-8797 %           98";
            // todo universal test case for valid and invaled states
            string message;
            // act
            var validResult1 = Validator.ActionsControl(validString1, out message);
            var validResult2 = Validator.ActionsControl(validString2, out message);
            var validResult3 = Validator.ActionsControl(validString3, out message);
            var validResult4 = Validator.ActionsControl(validString4, out message);
            var invalidResult1 = Validator.ActionsControl(invalidString1, out message);
            var invalidResult2 = Validator.ActionsControl(invalidString2, out message);
            var invalidResult3 = Validator.ActionsControl(invalidString3, out message);
        }
    }
}
