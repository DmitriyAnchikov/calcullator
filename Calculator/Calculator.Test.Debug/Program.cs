﻿using Calculator.Common;
using Calculator.Data;
using System;

namespace Calculator.Test.Debug
{
    class Program
    {
        static void Main(string[] args)
        {
            //ParserTest.TakeNextVariableTest();
            //ParserTest.TryTakeNextSignTest();
            //ParserTest.FindFirstBeginingBracketIndexTest();
            //ParserTest.FindEndingBracketIndexTest();

            //ValidatorTest.BracketControlTest();
            //ValidatorTest.ActionsControlTest();

            //Variable stmnt;
            //bool result = Variable.TryStatementParse("a=2* true", out stmnt);

            // arrange
            CalculatorTest.CalculateLogicalVariablesTest();
        }
        
        
    }
}
