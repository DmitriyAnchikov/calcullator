﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Calculator.Common
{
    [DataContract]
    public class Statement
    {
        [DataMember]
        public Variable Head { get; set; }
        [DataMember]
        public Expression Body { get; set; }
        [DataMember]
        public StatementStatus Status { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Tryies to parse string to instance of Statement class and calculate it
        /// </summary>
        /// <param name="statement">Input string</param>
        /// <param name="stmnt">Output Statement instance</param>
        /// <returns>true if input string has been parsed or false if not</returns>
        public static bool TryParse(string statement, out Statement stmnt)
        {
            stmnt = new Statement() { Status = StatementStatus.NotParsed };
                
            if (String.IsNullOrEmpty(statement) || String.IsNullOrWhiteSpace(statement))
            {
                stmnt.ErrorMessage = "Statement cann't be an empty";
                return false;
            }
            var separatorIndex = statement.IndexOf('=');
            if (separatorIndex <= 0)
            {
                stmnt.ErrorMessage = "Statement must have left and right sides shared by '='-char.";
                return false;
            }
            var headString = statement.Substring(0, separatorIndex).Trim();
            if (!Validator.IsVariableName(headString))
            {
                stmnt.ErrorMessage = "Statement must starts with the variable name, e.g.: 'A', 'x', 'y5', 'Alpha', '_$erq'. First character cannot be a digit.";
                return false;
            }
            Expression newExp;
            if (!Expression.TryParse(statement.Substring(separatorIndex + 1), out newExp))
            {
                stmnt.ErrorMessage = newExp.ErrorMessage;
                stmnt.Status = newExp.Status;
                return false;
            }
            if (newExp.GetAllNamedOperandsRecursive().Any())
            {
                stmnt.Status = StatementStatus.WaitForResolving;
                return true;
            }
            dynamic result;
            if (!newExp.TryCalculate(out result))
            {
                stmnt.ErrorMessage = newExp.ErrorMessage;
                stmnt.Status = newExp.Status;
            }
            else
            {
                stmnt.Status = StatementStatus.Calculated;
            }
            stmnt.Head = new Variable(VarScope.Global, headString, result);
            stmnt.Head.Value = stmnt.Body;
            return true;
        }

    }
}
