﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;

namespace Calculator.Common
{
    [DataContract]
    public class Variable
    {
        #region Properties
        [DataMember]
        public VarScope Scope { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public object Value { get; set; }
        [DataMember]
        public Type DataType
        {
            get
            {
                if (Value != null)
                {
                    return Value.GetType();
                }
                return null;
            }
        }
        public string Message { get; set; }
        public StatementStatus Status { get; set; }
        public Expression Expression {get;set;}
        #endregion

        // ctor
        public Variable(VarScope scope = VarScope.Global, string name = null, object value = null, Expression exp = null) 
        {
            Scope = scope;
            Name = name;
            Value = value;
            Expression = exp;
        }

        #region Methods
        public ExpressionPriority GetPriority()
        {
            return Expression == null ? ExpressionPriority.Multiplicative : Expression.GetPriority();
        }
        /// <summary>
        /// Tries parse input expression string to the instance of Variable class
        /// </summary>
        /// <param name="inputString">string with name of variable and theirs value or expression</param>
        /// <param name="newVar">parsed new variable instance</param>
        /// <returns>true if parsing is successful or false if it fails</returns>
        public static bool TryParse(string inputString, out Variable newVar)
        {
            string str = inputString.Trim();
            newVar = new Variable();
            if (Validator.IsNumericData(str) )
            {
                double value;
                if(!double.TryParse(str, NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out value))
                {
                    newVar.Status = StatementStatus.IncorrectData;
                    newVar.Message = String.Format("Can not parse to numeric format data: '{0}'.", str);  
                    return false;
                }
                newVar = new Variable(VarScope.Local, null, value);
                return true;
            }
            bool result;
            if (bool.TryParse(str.ToLower(), out  result))
            {
                newVar = new Variable(VarScope.Local, null, result);
                return true;
            }
            if (Validator.IsVariableName(str))
            {
                newVar = new Variable(VarScope.Global, str, null);
                return true;
            }
            newVar.Message = String.Format("Can not recognize data: '{0}'.",str);
            newVar.Status = StatementStatus.IncorrectData;
            return false;
        }
        /// <summary>
        /// Tries calculate the result of expression
        /// </summary>
        /// <returns>true if succes or false if fail</returns>
        public bool TryCalculate()
        {
            try
            {
                if (Expression == null)
                {
                    Message = String.Format("Attempt to calculate static data: '{0}'.", this.ToString());
                    return false;
                }
                object result;
                if (!Expression.TryCalculate(out result))
                {
                    Status = Expression.Status;
                    Message = Expression.ErrorMessage;
                    return false;
                }
                Message = null;
                Value = result;
                Status = StatementStatus.Calculated;
                return true;
            }
            catch(Exception e)
            {
                // todo logic for exception hadling
                throw new Exception("Calculation Error.",e);
            }
        }
        /// <summary>
        /// Tryies to parse string to instance of Variable class and calculate it
        /// </summary>
        /// <param name="statement">Input string</param>
        /// <param name="stmnt">Output Variable instance</param>
        /// <returns>true if input string has been parsed or false if not</returns>
        public static bool TryStatementParse(string statement, out Variable stmnt)
        {
            stmnt = new Variable() { Status = StatementStatus.NotParsed };

            if (String.IsNullOrEmpty(statement) || String.IsNullOrWhiteSpace(statement))
            {
                stmnt.Message = "Statement cann't be an empty";
                return false;
            }
            var separatorIndex = statement.IndexOf('=');
            if (separatorIndex <= 0)
            {
                stmnt.Message = "Statement must have left and right sides divided by '='-char.";
                return false;
            }
            var headString = statement.Substring(0, separatorIndex).Trim();
            if (!Validator.IsVariableName(headString))      // Incorrect left side of statement
            {
                stmnt.Message = "Statement must starts with the variable name, e.g.: 'A', 'x', 'y5', 'Alpha', '_$erq'. First character cannot be a digit.";
                return false;
            }
            Expression newExp;
            if (!Expression.TryParse(statement.Substring(separatorIndex + 1), out newExp))  // Parsing to Expression instance
            {
                stmnt.Message = newExp.ErrorMessage;
                stmnt.Status = newExp.Status;
                return false;
            }
            stmnt = new Variable(VarScope.Global, headString, null, newExp);
            Variables operands;
            if ((operands = newExp.GetAllNamedOperandsRecursive()).Any())       // undefined variables
            {
                stmnt.Status = StatementStatus.WaitForResolving;
                bool moreThanOne = operands.Count > 1;
                stmnt.Message = String.Format("Operand{0} {1} {2} undefined.", moreThanOne ? "s" : "",
                    String.Format("'{0}'", String.Join("', '", operands.Select(o => o.Name))), moreThanOne ? "are" : "is");
                return true;
            }
            if (!stmnt.TryCalculate())
            {
                // todo some logic
                return false;
            }
            return true;
        }
        public bool AssignTo(string value)
        {
            if (Validator.IsNumericData(value))
            {
                double doubleValue;
                if (!double.TryParse(value, NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out doubleValue))
                {
                    return false;
                }
                Value = doubleValue;
                return true;
            }
            bool boolValue;
            if (bool.TryParse(value.ToLower(), out  boolValue))
            {
                Value = boolValue;
                return true;
            }
            return false;
        }
        public override string ToString()
        {
            return String.IsNullOrEmpty(Name) ?
                Value == null ?
                    Expression == null? "'Unnamed variable'" : String.Format("({0})", Expression.ToString()) :
                    Value.ToString() :
                Value == null ?
                    Name :
                    String.Format("{0}(={1})", Name, Value.ToString());
        }
        #endregion
    }
    [CollectionDataContract]
    public class Variables : List<Variable>
    { }
    public enum VarScope
    {
        Global,
        Local
    }
}
