﻿using System;
using System.Collections.Generic;

namespace Calculator.Common
{
    public static class Actions
    {
        // high level of trust => is needed high level of validation
        public static Dictionary<string, Func<dynamic, dynamic, dynamic>> Binary = new Dictionary<string, Func<dynamic, dynamic, dynamic>>
        {
            {"+", (f, s) => f + s},
            {"*", (f, s) => f * s},
            {"-", (f, s) => f - s},
            {"/", (f, s) => f / s},
            {"%", (f, s) => f % s},
            {"==", (f, s) => f == s},
            {"!=", (f, s) => f != s},
            {">", (f, s) => f > s},
            {">=", (f, s) => f >= s},
            {"<", (f, s) => f < s},
            {"<=", (f, s) => f <= s},
            {"&&", (f, s) => f && s},
            {"||", (f, s) => f || s},
        };

        public static Dictionary<string, Func<dynamic, dynamic>> Unary = new Dictionary<string, Func<dynamic, dynamic>>
        {
            {"+", f => f},
            {"-", f => -f},
            {"!", f => !f}
        };
    }
}
