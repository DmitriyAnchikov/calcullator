﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Calculator.Common
{
    public class Expression
    {
        #region Properties
        [DataMember]
        public Variable First { get; set; }
        [DataMember]
        public Variable Second { get; set; }
        [DataMember]
        public string Operator;
        public string ErrorMessage { get; set; }
        public string StringForm { get; set; }
        public StatementStatus Status { get; set; }
        public ExpressionPriority GetPriority()
        {
            if (First == null && SyntaxConst.AllUnarySigns.Contains(Operator))
            {
                return ExpressionPriority.Primary;
            }
            return SyntaxConst.GetPriority(Operator);
        }
        #endregion

        // ctor
        public Expression(Variable first = null, Variable second = null, string sign = null)
        {
            //Operands = new Variables();
            //Operations = new Operations();
            First = first;
            Second = second;
            Operator = sign;
        }
        
        
        #region Public methods
        /// <summary>
        /// Tries parse string expression to instance of Expression-class
        /// </summary>
        /// <param name="expression">string</param>
        /// <param name="exp">output instance of Expression-class</param>
        /// <returns>true if succes or false if it fails</returns>
        public static bool TryParse(string expression, out Expression exp)
        {
            exp = new Expression() { Status = StatementStatus.NotParsed };
            string message;
            
            if (String.IsNullOrEmpty(expression) || String.IsNullOrWhiteSpace(expression))
            {
                exp.ErrorMessage = "Expression can not be an empty";
                return false;
            }
            if (!Validator.BracketControl(expression, out message))
            {
                exp.Status = StatementStatus.IncorrectFormat;
                exp.ErrorMessage = message ;
                return false;
            }
            if (!Validator.ActionsControl(expression, out message))
            {
                exp.Status = StatementStatus.IncorrectFormat;
                exp.ErrorMessage = message;
                return false;
            }
            if (!TryDistribute(expression, ref exp))
            {
                return false;
            }
            exp.StringForm = expression;
            exp.Status = StatementStatus.Parsed;
            return true;
        }
        /// <summary>
        /// Returnes all named variables in expression and in all child expressions
        /// </summary>
        /// <returns>collection of Variable instances</returns>
        public Variables GetAllNamedOperandsRecursive()
        {
            Variables allOperands = new Variables();
            if (First != null && !String.IsNullOrEmpty(First.Name))
            {
                allOperands.Add(First);
            }
            if ( Second != null && !String.IsNullOrEmpty(Second.Name))
            {
                allOperands.Add(Second);
            }
            if (First != null && First.Expression != null)
            {
                var firstExpNamedVariables = First.Expression.GetAllNamedOperandsRecursive();
                if (firstExpNamedVariables.Any())
                {
                    allOperands.AddRange(firstExpNamedVariables);
                }
            }
            if (Second != null && Second.Expression != null)
            {
                var secondExpNamedVariables = Second.Expression.GetAllNamedOperandsRecursive();
                if (secondExpNamedVariables.Any())
                {
                    allOperands.AddRange(secondExpNamedVariables);
                }
            }
            return allOperands;
        }
        /// <summary>
        /// Tryies to calculate the result of parsed instance of Expression
        /// </summary>
        /// <param name="result">non-nullable result of calculation if success or null if fail</param>
        /// <returns>true if calculation is calculated or false if failed</returns>
        public bool TryCalculate(out dynamic result)
        {
            result = null;
            if (!Validate())     // internal rules valdation
            {
                return false;
            }
            if (First == null && Second != null && (Operator == null || SyntaxConst.AllUnarySigns.Contains(Operator)))
            {
                if (Second.Value == null && !Second.TryCalculate())
                {
                    ErrorMessage = Second.Message;
                    Status = Second.Status;
                    return false;
                }
                Status = StatementStatus.Calculated;
                result = Actions.Unary[Operator == null ? "+" : Operator](Second.Value);
                return true;     // Calculating
            } 
            if (Operator != null && !Actions.Binary.ContainsKey(Operator))     // syntax validation
            {
                Status = StatementStatus.ValidationFailed;
                ErrorMessage = String.Format("Undefined action '{0}'.", Operator);
                return false;
            }
            if ((First.Value == null && !String.IsNullOrEmpty(First.Name)) || (Second.Value == null && !String.IsNullOrEmpty(Second.Name))) // null-validation
            {
                Status = StatementStatus.WaitForResolving;
                ErrorMessage = String.Format("Awaited variables: '{0}' or '{1}'", First.Name, Second.Name);
                return false;
            }
            if (First.Value == null && String.IsNullOrEmpty(First.Name) && First.Expression != null)        // calculating value
            {
                if (!First.TryCalculate())
                {
                    Status = First.Status;
                    ErrorMessage = First.Message;
                    return false;
                }
            }
            if (Second.Value == null && String.IsNullOrEmpty(Second.Name) && Second.Expression != null) // calculating value
            {
                if (!Second.TryCalculate())
                {
                    Status = First.Status;
                    ErrorMessage = First.Message;
                    return false;
                }
            }
            if (First.DataType != Second.DataType)      // Type mismatch
            {
                Status = StatementStatus.IncorrectData;
                ErrorMessage = String.Format("Type of variales mismatch: '{0}' and '{1}'", First.DataType, Second.DataType);
                return false;
            }
            Status = StatementStatus.Calculated;
            result = Actions.Binary[Operator](First.Value, Second.Value);
            return true;     // Calculating
        }
        public override string ToString()
        {
            string toString = First == null && Second == null ? "Empty Expression" :
                String.Format("{0} {1} {2}", First == null ? "" : First.ToString(), Operator, Second == null ? "" : Second.ToString());
            return toString;
        }
        #endregion

        #region Private extra methods
        /// <summary>
        /// Validates the operation and operands 
        /// </summary>
        /// <returns>true if operation is valid or false if not</returns>
        static bool TryDistribute(string expression, ref Expression exp)
        {
            var variable = string.Empty;
            int offset = 0;

            if (!TryProcessFirstVariable(expression, ref exp, ref offset))
            {
                return false;
            }

            while (!String.IsNullOrEmpty(variable = Parser.TakeNextVariable(expression, offset)))
            {
                Variable newVar = null;
                if (!TryProcessNextVariable(expression, variable, ref exp, ref offset, out newVar))
                {
                    return false;
                }
                ProcessNextSign(expression, newVar, ref exp, ref offset);
            }
            string remainPart = expression.Substring(offset).Trim();
            if (!String.IsNullOrEmpty(remainPart))
            {
                exp.ErrorMessage = String.Format("Cannot parse part of expression: {0}.", remainPart);
                return false;
            }
            return true;
        }
        static bool TryProcessFirstVariable(string expression, ref Expression exp, ref int offset)
        {
            string variable = Parser.TakeNextVariable(expression, offset);
            if (!String.IsNullOrEmpty(variable.Trim()))   // if first variable is not empty case
            {
                return true;
            }
            string nextSign;
            int signIndex;
            if (!Parser.TryTakeNextSign(expression, offset, out nextSign, out signIndex) || !SyntaxConst.AllUnarySigns.Contains(nextSign))      //  if expression not contains any signs or if first sign is not additive
            {
                exp.ErrorMessage = String.Format("Variables or additive signs are expected at the begining of expression: {0}.", expression);
                return false;
            }
            offset = signIndex + nextSign.Length;
            variable = Parser.TakeNextVariable(expression, offset);
            Variable newVar;
            if (!TryProcessNextVariable(expression, variable, ref exp, ref offset, out newVar))     // not processable  first non-empty variable case
            {
                return false;
            }
            newVar = new Variable(VarScope.Local, null, null, new Expression(null, newVar, nextSign));
            ProcessNextSign(expression, newVar, ref exp, ref offset);
            return true;
        }
        static bool TryProcessNextVariable(string expression, string variable, ref Expression exp, ref int offset, out Variable newVar)
        {
            char? bracketChar = null;
            newVar = null;
            int startBracketIndex = Parser.FindFirstOpenBracketIndex(expression, out bracketChar, offset, variable.Length);
            if (startBracketIndex > -1)
            {
                int endBracketIndex = Parser.FindClosedBracketIndex(expression, bracketChar.Value, startBracketIndex);
                string substring = expression.Substring(startBracketIndex + 1, endBracketIndex - startBracketIndex - 1);
                Expression newExp = null;
                if (!TryParse(substring, out newExp))
                {
                    exp = newExp;
                    return false;
                }
                newVar = new Variable(VarScope.Local, null, null, newExp);
                offset = endBracketIndex + 1;
                return true;
            }
            if (!Variable.TryParse(variable, out newVar))
            {
                exp.Status = newVar.Status;
                exp.ErrorMessage = newVar.Message;
                return false;
            }
            offset = offset + variable.Length;
            return true;
        }
        static void ProcessNextSign(string expression, Variable newVar, ref Expression exp, ref int offset)
        {
            string nextSign = "";
            int signIndex = -1;
            Expression incompleteExpression = exp.GetIncompleteExpression();
            if (!Parser.TryTakeNextSign(expression, offset, out nextSign, out signIndex))
            {
                incompleteExpression.Second = newVar;
                return;
            }
            offset = signIndex + nextSign.Length;
            if (exp.First == null && exp.Operator == null)
            {
                exp.First = newVar;
                exp.Operator = nextSign;
                return;
            }
            if (incompleteExpression.GetPriority() > SyntaxConst.GetPriority(nextSign))
            {
                incompleteExpression.First = new Variable(VarScope.Local, null, null, new Expression(incompleteExpression.First, newVar, incompleteExpression.Operator));
                incompleteExpression.Operator = nextSign;
                return;
            }
            incompleteExpression.Second = new Variable(VarScope.Local, null, null, new Expression(newVar, null, nextSign));
        }
        Expression GetIncompleteExpression()
        {
            return Second == null ? this : Second.Expression.GetIncompleteExpression();
        }
        Variable GetNullVariable()
        {
            return Second == null ? Second : Second.Expression.GetNullVariable();
        }
        bool Validate()
        {
            ErrorMessage = null;
            if (First == null && Second == null)
            {
                ErrorMessage = String.Format("{0} operand is NULL: {1}", First == null ? "First" : "Second", this.ToString());
                return false;
            }
            if (Operator == "/" && (dynamic)Second.Value == 0)
            {
                ErrorMessage = String.Format("Division by zero attempted: {0} = 0.", Second.Name);
                return false;
            }
            return true;
        }
        #endregion
    }
}
