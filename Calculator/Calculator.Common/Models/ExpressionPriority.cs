﻿
namespace Calculator.Common
{
    public enum ExpressionPriority
    {
        Unknown = 1,
        Primary = 0,
        Multiplicative = -1,
        Additive = -2,
        Relative = -3,
        Equivality = -4,
        Logical = -5
    }
}
