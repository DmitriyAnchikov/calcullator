﻿using System.Runtime.Serialization;

namespace Calculator.Common
{
    [DataContract]
    public enum StatementStatus
    {
        [EnumMember]
        Undefined = -10,
        [EnumMember]
        Calculated = 0,
        [EnumMember]
        NotParsed = 1,
        [EnumMember]
        ValidationFailed = 2,
        [EnumMember]
        Parsed = 3,
        [EnumMember]
        WaitForResolving = 4,
        [EnumMember]
        UnAutorized = 5,
        [EnumMember]
        IncorrectFormat = 6,
        [EnumMember]
        IncorrectData = 7,
        [EnumMember]
        InternalError = -1
    }
}
