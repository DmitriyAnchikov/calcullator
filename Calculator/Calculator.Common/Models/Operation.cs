﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Calculator.Common
{
    [DataContract]
    public class Operation 
    {
        [DataMember]
        public Variable First { get; set; }
        [DataMember]
        public Variable Second { get; set; }
        [DataMember]
        public string Operator;
        public StatementStatus Status { get; set; }

        public Operation(Variable varFirst, Variable varSecond, string sign)
        {
            First = varFirst;
            Second = varSecond;
            Operator = sign;
        }
        
        /// Calculates the result of operation
        /// </summary>
        /// <returns>Numeric type</returns>
        public dynamic Calculate(out string message)
        {
            message = null;
            if (!Validate(out message))     // internal rules valdation
            {
                Status = StatementStatus.ValidationFailed;
                return null;
            }
            if (!Actions.Binary.ContainsKey(Operator) )     // syntax validation
            {
                Status = StatementStatus.ValidationFailed;
                message = String.Format("Undefined action '{0}'.", Operator);
                return null;
            }
            if (First.Value == null && Second.Value == null)        // null-validation
            {
                Status = StatementStatus.WaitForResolving;
                message = String.Format("Awaited variables: '{0}' or '{1}'", First.Name, Second.Name);
                return null;
            }
            if (First.DataType != Second.DataType)      // Type mismatch
            {
                Status = StatementStatus.IncorrectData;
                message = String.Format("Type of variales mismatch: '{0}' and '{1}'", First.DataType, Second.DataType);
                return null;
            }
            Status = StatementStatus.Calculated;
            return Actions.Binary[Operator](First.Value, Second.Value);
        }
        /// <summary>
        /// Validates the operation and operands 
        /// </summary>
        /// <returns>true if operation is valid or false if not</returns>
        bool Validate(out string Message)
        {
            Message = null;
            if (First == null || Second == null)
            {
                Message = String.Format("{0} operand is NULL: {1}", First == null ? "First" : "Second", this.ToString());
            }
            if (Operator == "/" && (dynamic)Second.Value == 0)
            {
                Message = String.Format("Division by zero attempted: {0} = 0.", Second.Name) ;
                return false;
            }
            return true;
        }
        public override string ToString()
        {
            return String.Format("{0} {1} {2}", First.ToString(), Operator, Second.ToString()); ;
        }
    }
    [CollectionDataContract]
    public class Operations : List<Operation>
    {
        //public bool TryCalculate(Variables operands, out object result, out string message)
        //{
        //    message = null;
        //    result = null;
        //    var multiplicativeOperations = this.Where(o => SyntaxConst.MultiplicativeSigns.Contains(o.Operator));
        //    foreach (var operation in this)
        //    {
        //        var value = operation.Calculate(out message);
        //        if (value == null)
        //        {
        //            return false;
        //        }

        //    }
        //}
    }
}
