﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Calculator.Common;

namespace Calculator.Common
{
    public class CalculatorException : Exception
    {
        public StatementStatus Status { get; set; }
    }
}