﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator.Common
{
    public static class Validator
    {
        /// <summary>
        /// Validates by specific symbols filtering 
        /// </summary>
        /// <param name="part"></param>
        /// <returns>true if incoming string has not any specific symbols, or false if it has</returns>
        public static bool IsVariableName(string part)
        {
            if (String.IsNullOrEmpty(part) || String.IsNullOrWhiteSpace(part))
            {
                return false;
            }
            if (!SyntaxConst.Digits.Contains(part[0]) && part.All(c => !SyntaxConst.ContraintChars.Contains(c)))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Validates by digits filtering in string
        /// </summary>
        /// <param name="part"></param>
        /// <returns>true if incoming string part has numeric form, or false if it is not</returns>
        public static bool IsNumericData(string part)
        {
            if (part.All(c => SyntaxConst.DigitsDotComma.Contains(c)) && part.Where(c=> SyntaxConst.DotComma.Contains(c)).Count() < 2)
                return true;
            return false;
        }
        /// <summary>
        /// Validates on correct bracket format of input expression-string
        /// </summary>
        /// <param name="expression">input string</param>
        /// <returns>true if correct format or false if incorrect</returns>
        public static bool BracketControl(string expression, out string message)
        {
            message = null;
            if (SyntaxConst.Brackets.Any(b => expression.Where(c => c == b.Key).Count() != expression.Where(c => c == b.Value).Count()))//OnBracket(expression, b.Key, b.Value)))
            {
                message = String.Format("Unbalances brackets: '{0}", expression);
                return false;
            }
            return OnBracket(expression);
        }
        /// <summary>
        /// Validates expression with brackets on balance and sequence format
        /// </summary>
        /// <param name="expression">input string</param>
        /// <returns>true if expression with bracket is correct or false if incorrect</returns>
        static bool OnBracket(string expression)
        {
            
            int openBracketCharOffset = 0;
            int closeBracketCharOffSet = 0;
            int bracketStartIndex = 0;
            int bracketEndIndex = 0;
            char? start;
            do
            {
                if (SyntaxConst.Brackets.Any(b => !IsInitialBracketSequenceCorrect(expression, b.Key, openBracketCharOffset)))
                {
                    return false;
                }
                if ((bracketStartIndex = Parser.FindFirstOpenBracketIndex(expression, out start, openBracketCharOffset)) > -1)
                {
                    bracketEndIndex = Parser.FindClosedBracketIndex(expression, start.Value, bracketStartIndex);
                    if ((bracketStartIndex >= 0 && bracketEndIndex < 0) || bracketEndIndex < bracketStartIndex) // mmismatch open and closed brackets
                    {
                        return false;
                    }
                    if (!OnBracket(expression.Substring(bracketStartIndex + 1, bracketEndIndex - bracketStartIndex - 1)))   // recursive calling to the substring into brackets
                    {
                        return false;
                    }
                    openBracketCharOffset = bracketEndIndex + 1;
                    closeBracketCharOffSet = bracketEndIndex + 1;
                }
            }
            while (bracketStartIndex > -1 && bracketEndIndex > -1);
            return true;
        }
        static bool IsInitialBracketSequenceCorrect(string expression, char openBracket, int offset)
        {
            int openBracketIndex = expression.IndexOf(openBracket, offset);
            int closeBracketIndex = expression.IndexOf(SyntaxConst.Brackets[openBracket], offset);
            return (openBracketIndex == -1 && closeBracketIndex == -1) || openBracketIndex < closeBracketIndex;
        }
        /// <summary>
        /// Validates on correct action signs format in input string
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static bool ActionsControl(string expression, out string message)
        {
            message = null;
            var additiveOperands = expression.Split(SyntaxConst.AllUnarySigns.ToArray(), StringSplitOptions.None);
            var operands = expression.Split(SyntaxConst.AllSigns.Except(SyntaxConst.AllUnarySigns).ToArray(), StringSplitOptions.None);
            //var operands = expression.Split(SyntaxConst.AllSigns.ToArray(), StringSplitOptions.None);
            if (additiveOperands.Skip(1).Any(s => String.IsNullOrEmpty(s.Trim())) || operands.Any(s => String.IsNullOrEmpty(s.Trim())))       // validation for doubling signs
            {
                message = String.Format("Double signs were found: '{0}", expression);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Validates expression string on occurance any sign in stringCollection</string>
        /// </summary>
        /// <param name="expression">input string</param>
        /// <param name="signs">string collection</param>
        /// <returns>true if input string contains any sign in string collection</returns>
        static bool ContainsSigns(this string expression, List<string> signs)
        {
            return signs.Any(s => expression.Contains(s));
        }
    }
}
