﻿using System.Linq;

namespace Calculator.Common
{
    public static class Parser
    {
        
        /// <summary>
        /// Takes next variable
        /// </summary>
        /// <param name="expression">input valid string</param>
        /// <param name="offset">start position for searching</param>
        /// <returns>next variable string before next sign/the end of string or empty string </returns>
        public static string TakeNextVariable(string expression, int offset)
        {
            if (offset >= expression.Length)        // incorrect offset
            {
                return string.Empty;
            }
            int min = expression.Length;

            foreach(var sign in SyntaxConst.AllSigns)
            {
                var index = expression.IndexOf(sign, offset);
                if (index > -1 && min > index)
                {
                    min = index;
                }
            }
            return expression.Substring(offset, min - offset);
        }
        /// <summary>
        /// Tryies to take next math action sign from input string
        /// </summary>
        /// <param name="expression">input valid string</param>
        /// <param name="offset">start position for searching</param>
        /// <param name="nextSign">next math action sign if exist or empty string if it is not</param>
        /// <param name="signIndex">position of next sign if exist or -1 if it is mot</param>
        /// <returns></returns>
        public static bool TryTakeNextSign(string expression, int offset, out string nextSign, out int signIndex)
        {
            nextSign = "";
            if (offset >= expression.Length)    // incorrect offset value
            {
                signIndex = -1;
                return false;
            } 
            signIndex = expression.Length;
            
            foreach (var sign in SyntaxConst.AllSigns)
            {
                var index = expression.IndexOf(sign, offset);
                if (index > -1 && signIndex > index)
                {
                    signIndex = index;
                    nextSign = sign;
                }
            }
            if (signIndex == expression.Length)     // if sign is absent in expression
            {
                signIndex = -1;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Finds index of first opening bracket like '(', '[' or'{'
        /// </summary>
        /// <param name="expression">input string</param>
        /// <param name="letter">one characters if it exist or null if it is not</param>
        /// <param name="offset">start index for search</param>
        /// <returns>index of first open brackets-char</returns>
        public static int FindFirstOpenBracketIndex(string expression, out char? letter, int offset, int count = -1)
        {
            char[] chars = SyntaxConst.Brackets.Keys.ToArray();
            letter = null;
            int index = -1;
            foreach (var c in chars)
            {
                var cIndex = count > -1 ? expression.IndexOf(c, offset, count) : expression.IndexOf(c, offset);
                if (cIndex > -1 && (!letter.HasValue || index > cIndex))
                {
                    letter = c;
                    index = cIndex;
                }
            }
            return index;
        }
        /// <summary>
        /// Finds index of closing level-apropriate bracket-char 
        /// </summary>
        /// <param name="expression">input string</param>
        /// <param name="openBracket">character of brackets</param>
        /// <param name="offset">start index of open bracket-char</param>
        /// <returns>index of cosing bracket-char</returns>
        public static int FindClosedBracketIndex(string expression, char openBracket, int offset)
        {
            var closeBracket = SyntaxConst.Brackets[openBracket];
            int nextOpeningBracketIndex = 0;
            int nextClosingBracketIndex = 0;
            int OpenBracketOffSet = offset + 1;
            int CloseBracketOffSet = offset + 1;
            int balance = 1;
            do
            {
                nextOpeningBracketIndex = expression.IndexOf(openBracket, OpenBracketOffSet);
                nextClosingBracketIndex = expression.IndexOf(closeBracket, CloseBracketOffSet);
                if (nextOpeningBracketIndex < 0 || nextOpeningBracketIndex > nextClosingBracketIndex)
                {
                    balance--;
                }
                OpenBracketOffSet = nextOpeningBracketIndex + 1;
                CloseBracketOffSet = nextClosingBracketIndex + 1;
            }
            while (balance != 0);
            return nextClosingBracketIndex;
        }
        /// <summary>
        /// Extension method for trimming bracket-symbols like '(', '[', '{' from string
        /// </summary>
        /// <param name="expression"></param>
        /// <returns>trimmed string</returns>
        static string TrimBrackets(this string expression)
        {
            var q = expression;
            if (SyntaxConst.Brackets.ContainsKey(q[0]) || SyntaxConst.Brackets.ContainsValue(q[0]))
                q = q.Substring(1);
            if (SyntaxConst.Brackets.ContainsKey(q[q.Length - 1]) || SyntaxConst.Brackets.ContainsValue(q[q.Length - 1]))
                q = q.Substring(0, q.Length - 1);
            return q;
            //return expression.Trim(Brackets.Keys.Concat(Brackets.Values).ToArray());
        }
    }
}
