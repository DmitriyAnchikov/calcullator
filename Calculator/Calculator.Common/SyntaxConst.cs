﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator.Common
{
    public static class SyntaxConst
    {
        #region Fields
        public static Dictionary<char, char> Brackets = new Dictionary<char, char>() { { '(', ')' }, { '[', ']' }, { '{', '}' } };
        public static List<string> AdditiveSigns = new List<string>() { "+", "-", "–" };
        public static List<string> MultiplicativeSigns = new List<string>() { "*", "/", "%" };
        public static List<string> RelativeSigns = new List<string>() { "<=", ">=", "<", ">" };
        public static List<string> EquivalenceSigns = new List<string>() { "==", "!=" };
        public static List<string> LogicalSigns = new List<string>() { "&&", "||" };
        public static List<string> UnarySigns = new List<string>() { "!" };
        public static string SpecificChars = "\\^@№;:\"'~#? ";
        public static string DotComma = ".,";
        public static string Digits = "1234567890";
        public static string DigitsDotComma = string.Concat(DotComma, Digits);
        public static string BigLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public static string SmallLetters = "abcdefghijklmnopqrstuvwxyz";
        public static List<string> AriphmeticSigns = new List<string>(AdditiveSigns.Concat(MultiplicativeSigns));
        public static List<string> AllSigns = new List<string>(AriphmeticSigns.Concat(RelativeSigns).Concat(EquivalenceSigns).Concat(LogicalSigns).Concat(UnarySigns));
        public static string Letters = string.Concat(BigLetters, SmallLetters);
        public static string LettersAndDigits = string.Concat(Digits, Letters);
        public static string ContraintChars = String.Concat(SpecificChars
            , string.Join("", AllSigns), new string(Brackets.Keys.ToArray()), new string(Brackets.Values.ToArray()));
        public static string SignsLettersDigitsDotComma = String.Concat(string.Join("", AllSigns), DigitsDotComma, Letters);
        public static List<string> AllUnarySigns = new List<string>(AdditiveSigns.Concat(UnarySigns));
        #endregion

        public static ExpressionPriority GetPriority(string sign)
        {
            if (String.IsNullOrEmpty(sign))
            {
                return ExpressionPriority.Unknown;
            }
            if (SyntaxConst.MultiplicativeSigns.Contains(sign))
            {
                return ExpressionPriority.Multiplicative;
            }
            if (SyntaxConst.AdditiveSigns.Contains(sign))
            {
                return ExpressionPriority.Additive;
            }
            if (SyntaxConst.RelativeSigns.Contains(sign))
            {
                return ExpressionPriority.Relative;
            }
            if (SyntaxConst.EquivalenceSigns.Contains(sign))
            {
                return ExpressionPriority.Equivality;
            }
            if (SyntaxConst.LogicalSigns.Contains(sign))
            {
                return ExpressionPriority.Logical;
            }
            return ExpressionPriority.Unknown;
        }
    }
}
