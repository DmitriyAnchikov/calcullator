﻿using Calculator.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Calculator.Service;
using Calculator.Data;
using System.Threading.Tasks;
using System.Text;

namespace Calculator.Test.Unit
{
    public static class ExtraMethods
    {
        #region Extra - methods
        public static string CreateRandomValidStringVariable()
        {
            Random rnd = new Random();
            string validString = string.Empty;
            // variables forming
            int varLength = rnd.Next(1, 100);
            char[] validVariable = new char[varLength];
            validVariable[0] = SyntaxConst.Letters[rnd.Next(0, SyntaxConst.Letters.Length)];
            for (int j = 1; j < varLength; j++)
            {
                validVariable[j] = SyntaxConst.LettersAndDigits[rnd.Next(0, SyntaxConst.LettersAndDigits.Length)];
            }
            // Concat to expression
            return new string(validVariable);
        }
        public static string CreateRandomCloseValidStringExpression()
        {
            Random rnd = new Random();
            string validString = string.Empty;
            // variables forming
            int varLength = rnd.Next(1, 100);
            char[] validVariable = new char[varLength];
            validVariable[0] = SyntaxConst.Letters[rnd.Next(0, SyntaxConst.Letters.Length)];
            for (int j = 1; j < varLength; j++)
            {
                validVariable[j] = SyntaxConst.SignsLettersDigitsDotComma[rnd.Next(0, SyntaxConst.SignsLettersDigitsDotComma.Length)];
            }
            // Concat to expression
            return new string(validVariable);
        }
        public static string CreateNumericStringVariable()
        {
            Random rnd = new Random();
            return ((double)rnd.Next(0, 1000000000) / rnd.Next(0, 1000000000)).ToString();
        }
        public static List<bool> ActTryingStatementParseResults(List<string >expressions, ref Variables variables)
        {
            var validResults = new List<bool>();
            foreach (var exp in expressions)
            {
                Variable newVar = null;
                var result = Variable.TryStatementParse(exp, out newVar);
                validResults.Add(result);
                variables.Add(newVar);
            }
            return validResults.ToList();
        }
        public async static Task<List<string>> ActCalculateAndResolveResults(List<string> expressions, List<string> varNames)
        {
            var calculator = new Calculator.Service.Calculator(new VariableRepository(), new UserRepository());
            var results = new List<string>();
            var validResults = new List<bool>();
            string message = null;
            for(int i=0;i<expressions.Count;i++)
            {
                string value = null;
                var result = await calculator.Calculate(2,"test", expressions[i]);
                if (calculator.TryGetVariable(2, "test", varNames[i], out value, out message))
                {
                    results.Add(value);
                }
                else
                {
                    results.Add("");
                }
            }
            return results;
        }
        public static string CreateUniqueStringName(string startName="")
        {
            Random rnd = new Random();
            if (String.IsNullOrEmpty(startName))
            {
                startName = RandomString(rnd.Next(0, 10));
            }
            System.Threading.Thread.Sleep(rnd.Next(0, 100));
            return "r" + startName + RandomString(rnd.Next(0, 10)) + (DateTime.Now.Millisecond * rnd.Next(0, 1000000)).ToString() + RandomString(rnd.Next(0, 10));
        }
        public static string RandomString(int length)
        {
            if (length > 36)
                length = 36;
            else if (length < 1)
                length = 1;
            // see route config -> "VotingRoute"
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                          .Substring(0, length - 1)
                          .Replace("/", "_")
                          .Replace("+", "_")
                          .Replace("=", "_");
        }
        #endregion
    }
}
