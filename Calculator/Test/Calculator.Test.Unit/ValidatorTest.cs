﻿using Calculator.Common;
using NUnit.Framework;
using System;
using System.Linq;

namespace Calculator.Test.Unit
{
    [TestFixture]
    class ValidatorTest
    {
        [Test]
        public void IsVariableNameTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomValidStringVariable() ;
            string invalidString = validString + SyntaxConst.ContraintChars[rnd.Next(0, SyntaxConst.ContraintChars.Length)];
            // act
            var validResult = Validator.IsVariableName(validString);
            var invalidResult = Validator.IsVariableName(invalidString);

            //assert
            Assert.IsTrue(validResult);          // correct variable searching
            Assert.IsFalse(invalidResult);     // incorret string
        }

        [Test]
        public void IsNumericDataTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString1 = ExtraMethods.CreateNumericStringVariable();
            string validString2 = ExtraMethods.CreateNumericStringVariable();
            string invalidString1 = validString1 + SyntaxConst.Letters[rnd.Next(0, SyntaxConst.Letters.Length)];
            string invalidString2 = validString1 + SyntaxConst.AllSigns[rnd.Next(0, SyntaxConst.AllSigns.Count)];
            string invalidString3 = validString1 + SyntaxConst.SpecificChars[rnd.Next(0, SyntaxConst.SpecificChars.Count())];
            // act
            var validResult1 = Validator.IsNumericData(validString1);
            var validResult2 = Validator.IsNumericData(validString2);
            var invalidResult1 = Validator.IsNumericData(invalidString1);
            var invalidResult2 = Validator.IsNumericData(invalidString2);
            var invalidResult3 = Validator.IsNumericData(invalidString3);

            //assert
            String test = String.Format("String: {0}", validString1);
            Assert.IsTrue(validResult1, test);          // correct variable searching
            test = String.Format("String: {0}", validString2);
            Assert.IsTrue(validResult2, test);          // correct variable searching
            test = String.Format("String: {0}", invalidString1);
            Assert.IsFalse(invalidResult1, test);     // incorret string
            test = String.Format("String: {0}", invalidString2);
            Assert.IsFalse(invalidResult2, test);     // incorret string
            test = String.Format("String: {0}", invalidString3);
            Assert.IsFalse(invalidResult3, test);     // incorret string
        }

        [Test]
        public void BracketControlTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString1 =   "(()()()(()())())";
            string validString2 =   "(()()()(()())())()()(())";
            string invalidString1 = ")((()()()(()())())";
            string invalidString2 = "(()()()(()())())(";
            string invalidString3 = "(()()()(()())()) )(";
            string invalidString4 = "(()()()(()(])())";
            string invalidString5 = ")()(";
            // todo universal test case for valid and invaled states
            string message;
            // act
            var validResult1 = Validator.BracketControl(validString1, out message);
            var validResult2 = Validator.BracketControl(validString2, out message);
            var invalidResult1 = Validator.BracketControl(invalidString1, out message);
            var invalidResult2 = Validator.BracketControl(invalidString2, out message);
            var invalidResult3 = Validator.BracketControl(invalidString3, out message);
            var invalidResult4 = Validator.BracketControl(invalidString4, out message);
            var invalidResult5 = Validator.BracketControl(invalidString5, out message);

            //assert
            String test = String.Format("String: {0}", validString1);
            Assert.IsTrue(validResult1, test);          // correct variable searching
            test = String.Format("String: {0}", validString2);
            Assert.IsTrue(validResult2, test);          // correct variable searching
            test = String.Format("String: {0}", invalidString1);
            Assert.IsFalse(invalidResult1, test);     // incorret string
            test = String.Format("String: {0}", invalidString2);
            Assert.IsFalse(invalidResult2, test);     // incorret string
            test = String.Format("String: {0}", invalidString3);
            Assert.IsFalse(invalidResult3, test);     // incorret string
            test = String.Format("String: {0}", invalidString4);
            Assert.IsFalse(invalidResult4, test);     // incorret string
            test = String.Format("String: {0}", invalidString5);
            Assert.IsFalse(invalidResult5, test);     // incorret string
        }
        
        [Test]
        public void ActionsControlTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString1 = "x +2";
            string validString2 = "2 + 3";
            string validString3 = "A- B";
            string validString4 = "2 * 8";
            string invalidString1 = "2*/1";
            string invalidString2 = "4,87-- 87987";
            string invalidString3 = "--8797 %           98";
            // todo universal test case for valid and invaled states
            string message;
            // act
            var validResult1 = Validator.ActionsControl(validString1, out message);
            var validResult2 = Validator.ActionsControl(validString2, out message);
            var validResult3 = Validator.ActionsControl(validString3, out message);
            var validResult4 = Validator.ActionsControl(validString4, out message);
            var invalidResult1 = Validator.ActionsControl(invalidString1, out message);
            var invalidResult2 = Validator.ActionsControl(invalidString2, out message);
            var invalidResult3 = Validator.ActionsControl(invalidString3, out message);

            //assert
            String test = String.Format("String: {0}", validString1);
            Assert.IsTrue(validResult1, test);          // correct variable searching
            test = String.Format("String: {0}", validString2);
            Assert.IsTrue(validResult2, test);          // correct variable searching
            test = String.Format("String: {0}", validResult3);
            Assert.IsTrue(validResult3, test);     // incorret string
            test = String.Format("String: {0}", validResult4);
            Assert.IsTrue(validResult4, test);     // incorret string
            test = String.Format("String: {0}", invalidString1);
            Assert.IsFalse(invalidResult1, test);     // incorret string
            test = String.Format("String: {0}", invalidString2);
            Assert.IsFalse(invalidResult2, test);     // incorret string
            test = String.Format("String: {0}", invalidString3);
            Assert.IsFalse(invalidResult3, test);     // incorret string
        }
    }
}
