﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculator.Common;

namespace Calculator.Test.Unit
{
    [TestFixture]
    class ParserTest
    {
        [Test]
        public void TakeNextVariableTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomValidStringVariable();

            // act
            var validResult = Parser.TakeNextVariable(validString, 0);
            var invalidResult = Parser.TakeNextVariable(validString, rnd.Next(1, validString.Length));

            //assert
            Assert.AreEqual(validString, validResult);          // correct variable searching
            Assert.AreNotEqual(validString, invalidResult);     // corret offset parameter
        }
        [Test]
        public void TryTakeNextSignTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomValidStringVariable();
            var firstSign = SyntaxConst.AllSigns[rnd.Next(0, SyntaxConst.AllSigns.Count)];
            var firstSignIndex = validString.Length;
            validString += firstSign;
            validString += ExtraMethods.CreateRandomValidStringVariable();
            var secondSign = SyntaxConst.AllSigns[rnd.Next(0, SyntaxConst.AllSigns.Count)];
            var secondSignIndex = validString.Length;
            validString += secondSign;
            

            // act
            string firstSignResult;
            int firstSignIndexResult;
            string secondSignResult;
            int secondSignIndexResult;
            bool firstResult = Parser.TryTakeNextSign(validString, 0, out firstSignResult, out firstSignIndexResult);
            bool secondResult = Parser.TryTakeNextSign(validString, firstSignIndexResult + firstSignResult.Length, out secondSignResult, out secondSignIndexResult);
            
            //assert
            Assert.IsTrue(firstResult);     // correct result   
            Assert.IsTrue(secondResult);    // correct result  
            Assert.AreEqual(firstSign, firstSignResult);                // correct searched sign
            Assert.AreEqual(firstSignIndex, firstSignIndexResult);      // correct searched index
            Assert.AreEqual(secondSign, secondSignResult);              // correct offset parameter
            Assert.AreEqual(secondSignIndex, secondSignIndexResult);    // correct offset parameter
        }
        [Test]
        public void FindFirstBeginingBracketIndexTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomValidStringVariable(); // adding first valid variable
            var firstOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var firstOpenBracketIndex = validString.Length;
            validString += firstOpenBracket;    // adding first open bracket
            var secondVariable = ExtraMethods.CreateRandomValidStringVariable() + SyntaxConst.Brackets.Values.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)] + ExtraMethods.CreateRandomValidStringVariable();
                int secondVariableLength = secondVariable.Length;
            validString += secondVariable;  // adding close bracket into second variable
            var secondOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var secondOpenBracketIndex = validString.Length;
            validString += secondOpenBracket;   // adding second open bracket
            

            // act
            char? firstOpenBracketResult;
            char? secondOpenBracketResult;
            char? invalidSecondOpenBracketResult;
            int firstOpenBracketIndexResult = Parser.FindFirstOpenBracketIndex(validString, out firstOpenBracketResult, 0);
            int secondOpenBracketIndexResult = Parser.FindFirstOpenBracketIndex(validString, out secondOpenBracketResult, firstOpenBracketIndexResult+1);
            int invalidSecondOpenBracketIndexResult = Parser.FindFirstOpenBracketIndex(validString, out invalidSecondOpenBracketResult, firstOpenBracketIndexResult+1, secondVariableLength - rnd.Next(0, secondVariableLength - 1));
            
            //assert
            Assert.AreEqual(firstOpenBracket, firstOpenBracketResult);              // correct searching
            Assert.AreEqual(firstOpenBracketIndex, firstOpenBracketIndexResult);    // correct searching
            Assert.AreEqual(secondOpenBracket, secondOpenBracketResult);            // correct offset parameter
            Assert.AreEqual(secondOpenBracketIndex, secondOpenBracketIndexResult);  // correct offset parameter
            Assert.AreEqual(-1, invalidSecondOpenBracketIndexResult);   // correct count parameter
            Assert.IsNull(invalidSecondOpenBracketResult);              // correct count parameter
        }
        [Test]
        public void FindEndingBracketIndexTest([Range(1, 10)]int x)
        {
            // arrange
            Random rnd = new Random();
            string validString = ExtraMethods.CreateRandomCloseValidStringExpression(); // adding first valid variable
            var firstOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var firstOpenBracketIndex = validString.Length;
            validString += firstOpenBracket;    // adding first open bracket
            var secondVariable = ExtraMethods.CreateRandomCloseValidStringExpression();
            validString += secondVariable;  // adding second variable
            var secondOpenBracket = SyntaxConst.Brackets.Keys.ToArray()[rnd.Next(0, SyntaxConst.Brackets.Count)];
            var secondOpenBracketIndex = validString.Length;
            validString += secondOpenBracket;   // adding second open bracket
            var thirdVariable = ExtraMethods.CreateRandomCloseValidStringExpression();
            validString += thirdVariable;  // adding third variable
            var thirdCloseBracket = SyntaxConst.Brackets[secondOpenBracket];
            var thirdCloseBracketIndex = validString.Length;
            validString += thirdCloseBracket;   // adding third close bracket
            var fourthVariable = ExtraMethods.CreateRandomCloseValidStringExpression();
            validString += thirdVariable;  // adding fourth variable
            var fourthCloseBracket = SyntaxConst.Brackets[firstOpenBracket];
            var fourthCloseBracketIndex = validString.Length;
            validString += fourthCloseBracket;   // adding fourth close bracket
            
            // act
            int fourthCloseBracketIndexResult = Parser.FindClosedBracketIndex(validString, firstOpenBracket, firstOpenBracketIndex);
            int thirdOpenBracketIndexResult = Parser.FindClosedBracketIndex(validString, secondOpenBracket, secondOpenBracketIndex);
            int invalidThirdOpenBracketIndexResult = Parser.FindClosedBracketIndex(validString, SyntaxConst.Brackets.Keys.Except(new char[] {secondOpenBracket}).ToArray()[rnd.Next(0,2)], secondOpenBracketIndex);
            
            //assert
            Assert.AreEqual(fourthCloseBracketIndex, fourthCloseBracketIndexResult);    // correct searching
            Assert.AreEqual(thirdCloseBracketIndex, thirdOpenBracketIndexResult);  // correct offset parameter
            Assert.AreNotEqual(thirdCloseBracketIndex, invalidThirdOpenBracketIndexResult);  // correct open bracket char parameter
        }
        

    }
}
