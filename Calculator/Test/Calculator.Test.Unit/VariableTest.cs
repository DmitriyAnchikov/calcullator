﻿using Calculator.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Test.Unit
{
    [TestFixture]
    class VariableTest
    {
        [Test]
        public void TryStatementParseStaticNumericDataTest()
        {
            // arrange
            Random rnd = new Random();
            List<string> validExpressions = new List<string>()
                                                            { 
                                                                "A = 12/3",
                                                                "A = 0,3",
                                                                "A = -2.1    /4.2",
                                                                "A=((((2*(-7))/14+1)*2) +45  )/    45",
                                                                "A = 45/6",
                                                                "A = .3",
                                                                "A = 12."
                                                            };
            List<double> expectedResults = new List<double> { 4, 3, -0.5, 1,  (double)45/6,0.3,12.0};
            List<string> invalidExpressions = new List<string>()
                                                            { 
                                                                "",                                 // empty epression
                                                                "A == 12/3",                        // repeat of assignation-char
                                                                "A = (A – B) * / 4.2 + 23",         // doubling signs  
                                                                " = 0.3",                           // absent name of main variable
                                                                "A = (((((2*(-7))/14-1)*2) +45",    // unbalanced brackets
                                                                "A = 12/0"                          // division by zero
                                                            };
            List<string> throwsResults = new List<string>() {
                                                                "A = (A – B) * 4.2 + 23",           // awaitable variables
                                                                "Y = -2    /true",                  // bool & double
                                                                
                                                            };
            // act
            var validVariables = new Variables();
            var validResults = ExtraMethods.ActTryingStatementParseResults(validExpressions, ref validVariables);

            var invalidVariables = new Variables();
            var invalidResults = ExtraMethods.ActTryingStatementParseResults(invalidExpressions, ref invalidVariables);

            
            //assert
            String test;
            for (int i = 0; i < validVariables.Count; i++)
            {
                test = String.Format("Index: {0}", i);
                Assert.IsTrue(validResults[i], test);         // correct variable parsing
                test = String.Format("Var[{0}].Name: {1}", i, validVariables[i].Name);
                Assert.AreEqual("A", validVariables[i].Name, test);
                test = String.Format("Var[{0}].Value: {1}", i, validVariables[i].Value);
                Assert.AreEqual(expectedResults[i], validVariables[i].Value, test, double.Epsilon);
            }
            for (int i = 0; i < invalidVariables.Count; i++)
            {
                test = String.Format("Index: {0}", i);
                Assert.IsFalse(invalidResults[i], test);         // incorret string
                test = String.Format("Var[{0}].Value: {1}", i, invalidVariables[i].Value);
                Assert.AreEqual(null, invalidVariables[i].Value, test);
            }       
        }
        [Test]
        public void TryStatementParseStaticLogicalDataTest()
        {
            // arrange
            Random rnd = new Random();
            List<string> validExpressions = new List<string>()
                                                            { 
                                                                "A = tRue",
                                                                "C = !truE",           
                                                                "B = 0.3<=2",
                                                                "Y = -2.1    /4.2 < 7 && false",
                                                                "A=!(88<6) == false",
                                                                "A = 13 >=   1",
                                                                "A = ((((2*(-7))/14+1)*2) +45)/45 != 1"
                                                            };
            List<bool> expectedResults = new List<bool> { true, false, true, false, false, true, false };
            
            List<string> invalidExpressions = new List<string>()
                                                            { 
                                                                "A == 12/3",
                                                                "C = (A – B) * / 4.2 + 23",         // doubling signs  
                                                                "B = 0<<3",
                                                                "Y = -2    &| 4",
                                                                "A = (((((2*(-7))/14-1)*2) +45 !!= 1",    // unbalanced brackets
                                                                "A = 13^5"                          // unknown sign
                                                            };
            // act
            var validVariables = new Variables();
            var validResults = ExtraMethods.ActTryingStatementParseResults(validExpressions, ref validVariables);


            var invalidVariables = new Variables();
            var invalidResults = ExtraMethods.ActTryingStatementParseResults(invalidExpressions, ref invalidVariables);


            //assert
            string test;
            for (int i = 0; i < validVariables.Count; i++)
            {
                test = String.Format("Index: {0}", i);
                Assert.IsTrue(validResults[i], test);         // correct variable parsing
                test = String.Format("Var[{0}].Value: {0}", i, validVariables[i].Value);
                Assert.AreEqual(expectedResults[i], validVariables[i].Value, test);
            }
            for (int i = 0; i < invalidVariables.Count; i++)
            {
                test = String.Format("Index: {0}", i);
                Assert.IsFalse(invalidResults[i], test);         // incorret string
                test = String.Format("Var[{0}].Value: {0}", i, invalidVariables[i].Value);
                Assert.AreEqual(null, invalidVariables[i].Value, test);
            }    
        }
    }
}
