﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Client.Output
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new OutputClient();
            client.Start();
        }
    }
}
