﻿using Calculator.Client.Input.CalculatorServiceReference;

namespace Calculator.Client.Output
{
    public class OutputClient : ConsoleClientBase
    {
        //ctor
        public OutputClient()
        {
            TypeOfClient = "OUTPUT";
        }

        #region Service methods
        /// <summary>
        /// Outputs help info
        /// </summary>
        protected override void HelpInformation()
        {
            Write("Commands:");
            Write("clear - clearing command window", "quit/exit - quit from app"); // todo more commands 
            Write("\tYou can enter the name of variable, after that you will recieve the value of specific variable:");
            Write("A", "C", "b");
        }
        /// <summary>
        /// Handle user commands
        /// </summary>
        /// <param name="command">user command as string</param>
        protected override void Handle(string command)
        {
            switch (command.ToLower())
            {
                case "help":
                    HelpInformation();
                    return;
                case "clear":
                    Ready();
                    return;
                case "exit":
                case "quit":
                    Exit = true;
                    return;
                default:
                    string message = null;
                    GetVariable(command, out message);
                    Write(message);
                    return;
            }
        }
        #endregion

        #region Wcf - methods
        void GetVariable(string varName, out string message)
        {
            var client = new CalculatorServiceClient();
            var responce = client.GetStatementResult(new GetStatementResultRequest() { VariableName = varName, UserId = UserId, Password=  UserPassword });
            message = responce.Result == null? responce.Message : responce.Result.ToString();
        }
        #endregion
    }
}
