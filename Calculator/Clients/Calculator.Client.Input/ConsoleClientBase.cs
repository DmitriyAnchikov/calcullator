﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Calculator.Client.Input.CalculatorServiceReference;

namespace Calculator.Client
{
    public abstract class ConsoleClientBase
    {
        #region Fields
        protected string UserName;
        protected int UserId;
        protected string UserPassword;
        protected bool Exit = false;
        protected string TypeOfClient;
        #endregion

        #region Console-Methods
        protected void Ready()
        {
            Header();
            Write("Enter \"help\" for information");
        }
        protected void Header()
        {
            Clear();
            Console.WriteLine();
            Console.WriteLine("*** {0} CLIENT FOR CALCULATOR SERVICE ***", TypeOfClient.ToUpper());
            Console.WriteLine();
        }
        protected void Write(params string[] strings)
        {
            if (strings == null || !strings.Any())
            {
                return;
            }
            Console.WriteLine();
            foreach (var str in strings)
            {
                Console.WriteLine("\t{0}", str);
            }
            Console.WriteLine();
        }
        protected void Request()
        {
            Console.WriteLine();
            Console.Write("> ");
        }
        protected void Clear()
        {
            Console.Clear();
        }
        #endregion

        #region Service methods
        /// <summary>
        /// Main method for client app
        /// </summary>
        public void Start()
        {
            Header();
            if (Entrance())
            {
                Open();
            }
            Write("Press any key for exit");
            Console.ReadKey();
        }
        /// <summary>
        /// Reads string from console
        /// </summary>
        /// <returns>Non-empty and non-nullable string</returns>
        string Input()
        {
            string inputString = null;
            while (String.IsNullOrEmpty(inputString) || String.IsNullOrWhiteSpace(inputString))
            {
                Request();
                inputString = Console.ReadLine();
            };
            return inputString.Trim();
        }
        /// <summary>
        /// Reads name from console
        /// </summary>
        /// <returns>Valid string</returns>
        void EnterCredentials(out string name, out string password)
        {
            name = string.Empty;
            password = string.Empty;
            string error = null;
            do
            {
                Write(error ?? "");
                Write("Enter your NAME (3-15 characters):");
                name = Input();
            }
            while (!ValidateName(name, out error));

            do
            {
                Write(error ?? "");
                Write("Enter your PASSWORD (6-15 characters):");
                password = Input();
            }
            while (!ValidatePassword(password, out error));
        }
        /// <summary>
        /// Validates incoming string for the name
        /// </summary>
        /// <param name="name">input string</param>
        /// <param name="error">error message if exist or null</param>
        /// <returns>true if name valid or false if invalid</returns>
        bool ValidateName(string name, out string error)
        {
            error = null;
            string pattern = @"^[a-z0-9_-]{3,15}$";
            RegexOptions option = RegexOptions.IgnoreCase;
            Regex newReg = new Regex(pattern, option);
            if (!newReg.IsMatch(name))
            {
                error = "Incorrect name";
                return false;
            }
            return true;
        }
        bool ValidatePassword(string password, out string error)
        {
            error = null;
            string pattern = "^([a-zA-Z0-9@*#]{6,15})$";
            RegexOptions option = RegexOptions.IgnoreCase;
            Regex newReg = new Regex(pattern, option);
            if (!newReg.IsMatch(password))
            {
                error = "Incorrect password";
                return false;
            }
            return true;
        }
        /// <summary>
        /// Open the cycle for handling user commands
        /// </summary>
        void Open()
        {
            Ready();
            while (!Exit)
            {
                Handle(Input());
            }
        }
        /// <summary>
        /// Tryies login at user name
        /// </summary>
        /// <returns>true if it loged in successfully end or false if it failed</returns>
        bool Register(bool isRegistration)
        {
            RegistrationStatus status;
            string userName  = null;
            string userPass = null;
            string errorMessage = null;
            int userId;
            do
            {
                Header();
                if (!String.IsNullOrEmpty(errorMessage))
                {
                    Write(errorMessage);
                }
                EnterCredentials(out userName, out userPass);
                status = RegisterName(userName, userPass,isRegistration, out userId, out errorMessage);
                
            }
            while (status == RegistrationStatus.Unauthorized);

            if (status == RegistrationStatus.OkResult)
            {
                UserName = userName;
                UserPassword = userPass;
                UserId = userId;
                return true;
            }

            Write(errorMessage);
            return false;
        }
        bool Entrance()
        {
            Header();
            Write("Register new User or Log In?");
            var inputString = Input();
            while (inputString.ToLower() != "register" && inputString.ToLower() != "login")
            {
                Write("Enter 'register' or 'login'");
                inputString = Input();
            }
            if (inputString.ToLower() == "register")
            {
                return Register(true);
            }
            return Register(false);
        }
        #endregion

        #region Wcf - methods
        /// <summary>
        /// Registers userName in remote service
        /// </summary>
        /// <param name="name">user name</param>
        /// <param name="password">user password</param>
        /// <param name="errorMessage">message from service side</param>
        /// <returns>status of registration attempt from remote service</returns>
        RegistrationStatus RegisterName(string name, string password, bool isRegistration, out int userId, out string errorMessage)
        {
            userId = -1;
            errorMessage = null;
            try
            {
                var client = new CalculatorServiceClient();
                var responce = client.RegisterUser(new RegisterRequest() { UserName = name, Password = password, IsRegistration = isRegistration });
                errorMessage = responce.Message;
                userId = responce.UserId.HasValue? responce.UserId.Value : -1;
                return responce.Status;
            }
            catch (TimeoutException e)
            {
                errorMessage = "Service temporary unavailable. Try request later.";
                return RegistrationStatus.InternalError;
            }
            catch (Exception e)
            {
                errorMessage = "Internal Service Error. Try request later.";
                return RegistrationStatus.InternalError;
            }
        }
        #endregion

        #region Abstract methods
        abstract protected void Handle(string command);
        abstract protected void HelpInformation();
        #endregion
    }
}
