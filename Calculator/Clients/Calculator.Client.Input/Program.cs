﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Calculator.Client.Input.CalculatorServiceReference;

namespace Calculator.Client.Input
{
    class Program
    {
        static void Main(string[] args)
        {
            InputClient client = new InputClient();
            client.Start();
        }
    }
}
