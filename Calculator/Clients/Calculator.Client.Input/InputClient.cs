﻿using Calculator.Client.Input.CalculatorServiceReference;

namespace Calculator.Client.Input
{
    public class InputClient : ConsoleClientBase
    {
        //ctor
        public InputClient()
        {
            TypeOfClient = "INPUT";
        }

        #region Service methods
        /// <summary>
        /// Outputs help info
        /// </summary>
        protected override void HelpInformation()
        {
            Write("Commands:");
            Write("clear - clearing command window", "quit/exit - quit from app"); // todo more commands 
            Write("\tYou can enter the name of variable and assign to that value like write below:");
            Write("A = 12/3", "C = (A – B) * 4.2 + 23", "B = 0.3");
        }
        /// <summary>
        /// Handle user commands
        /// </summary>
        /// <param name="command">user command as string</param>
        protected override void Handle(string command)
        {
            switch (command.ToLower())
            {
                case "help":
                    HelpInformation();
                    return;
                case "clear":
                    Ready();
                    return;
                case "exit":
                case "quit":
                    Exit = true;
                    return;
                default:
                    string message = null;
                    PutStatement(command, out message);
                    Write(message);
                    return;
            }
        }
        #endregion

        #region Wcf - operations
        /// <summary>
        /// Tryis put statement to the remote service
        /// </summary>
        /// <param name="statement">input statement as string</param>
        void PutStatement(string statement, out string errorMessage)
        {
            var client = new CalculatorServiceClient();
            var responce = client.PutStatement(new PutStatementRequest() { Statement = statement, UserId = UserId, Password = UserPassword});
            errorMessage = responce.Message;
        }
        #endregion
    }
}
