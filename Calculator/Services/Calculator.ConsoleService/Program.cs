﻿using Calculator.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.ConsoleService
{
    class Program
    {
        static void Main(string[] args)
        {
            Type serviceType = typeof(CalculatorService);
            Uri serviceUri = new Uri("http://localhost:9088/CalculatorService.svc");
            ServiceHost host = new ServiceHost(serviceType, serviceUri);
            //ServiceHost host = new ServiceHost(serviceType, new Uri[] { });
            //host.AddServiceEndpoint(typeof(IEntityService), new NetTcpBinding(), "net.tcp://localhost:9000/EntityService");

            host.Open();
            Console.WriteLine("Marta, I Love You!");
            Console.ReadKey();
            host.Close();
        }
    }
}
