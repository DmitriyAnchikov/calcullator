﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Calculator.Common;

namespace Calculator.Service.Contracts
{
    [DataContract]
    public class PutStatementRequest
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Statement { get; set; }
    }
    [DataContract]
    public class PutStatementResponse
    {
        [DataMember]
        public StatementStatus Status { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
}