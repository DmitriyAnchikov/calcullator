﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Calculator.Service.Contracts
{
    [DataContract]
    public class RegisterRequest
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public bool IsRegistration { get; set; }
    }
    [DataContract]
    public class RegisterResponse
    {
        [DataMember]
        public RegistrationStatus Status { get; set; }
        [DataMember]
        public int? UserId { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
    [DataContract]
    public enum RegistrationStatus
    {
        [EnumMember]
        NotDefined = 0,
        [EnumMember]
        OkResult = 1,
        [EnumMember]
        Unauthorized = 2,
        [EnumMember]
        InternalError = -1
    }
}