﻿using Calculator.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Calculator.Service.Contracts
{
    [DataContract]
    public class GetStatementResultRequest
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string VariableName { get; set; }
    }
    [DataContract]
    public class GetStatementResultResponse
    {
        [DataMember]
        public StatementStatus Status { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public object Result { get; set; }
    }
}