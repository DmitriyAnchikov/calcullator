﻿using Calculator.Service.Contracts;
using System;
using System.ServiceModel;
using Calculator.Common;
using Calculator.Data;
using log4net;
using System.Threading.Tasks;

namespace Calculator.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, IncludeExceptionDetailInFaults = true)]
    public class CalculatorService : ICalculatorService
    {
        static readonly ILog Log = LogManager.GetLogger(typeof(CalculatorService));
        
        //IVariableRepository _variableRepository;
        //IUserRepository _userRepository;
        Calculator _calculator;

        public CalculatorService(IVariableRepository varRep, IUserRepository userRep)
        {
            _calculator = new Calculator(varRep, userRep);
        }
        public RegisterResponse RegisterUser(RegisterRequest request)
        {
            try
            {
                var userId = _calculator.Register(request.UserName, request.Password, request.IsRegistration);
                if (userId > 0)
                {
                    return new RegisterResponse() { Status = RegistrationStatus.OkResult, UserId = userId };
                }
                return new RegisterResponse() { Status = RegistrationStatus.Unauthorized, Message = request.IsRegistration? "This name is already registered. Try change name." : "UserName or Password is incorrect." };
            }
            catch(Exception ex)
            {
                Log.FatalFormat("Exception handling:\n\nMessage: {0},\nStackTrace: {1}{2}", ex.Message, ex.StackTrace, ex.InnerException == null ? "" : String.Format("InnerException\n\nMessage: {0},\nStackTrace: {1}{2}", ex.InnerException.Message,ex.InnerException.StackTrace));
                return new RegisterResponse() { Status = RegistrationStatus.InternalError, Message = String.Format("message: {0},\n\nstack: {1}", ex.Message, ex.StackTrace) };
            }
        }

        public async Task<PutStatementResponse> PutStatement(PutStatementRequest request)
        {
            try
            {
                Log.InfoFormat("Start Puting Statement {0} from {1}", request.Statement, request.UserId);
                string message = await _calculator.Calculate(request.UserId, request.Password, request.Statement);
                
                Log.InfoFormat("Parsing of {0} from {1} is failed: \n{2}", request.Statement, request.UserId, message);
                return new PutStatementResponse() { Message = message };
            }
            catch(Exception ex)
            {
                Log.FatalFormat("Exception handling:\n\nMessage: {0},\nStackTrace: {1}{2}", ex.Message, ex.StackTrace, ex.InnerException == null ? "" : String.Format("InnerException\n\nMessage: {0},\nStackTrace: {1}{2}", ex.InnerException.Message, ex.InnerException.StackTrace)); 
                return new PutStatementResponse() { Status = StatementStatus.InternalError, Message = "Try request later." };
            }
        }

        public GetStatementResultResponse GetStatementResult(GetStatementResultRequest request)
        {
            try
            {
                Log.InfoFormat("Start Getting Statement {0} from {1}", request.VariableName, request.UserId);
                string message;
                string result;
                if(_calculator.TryGetVariable(request.UserId, request.Password, request.VariableName, out result, out message))
                {
                    return new GetStatementResultResponse() { Result = result };
                }
                return new GetStatementResultResponse() { Message = message };
            }
            catch(Exception ex)
            {
                Log.FatalFormat("Exception handling:\n\nMessage: {0},\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return new GetStatementResultResponse() { Status = StatementStatus.InternalError, Message = "Try request later." };
            }
        }
    }
}
