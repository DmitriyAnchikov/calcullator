﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using Calculator.Data;

namespace Calculator.Service
{
    public class WcfNinjectModule : NinjectModule
    {
        
        public override void Load()
        {
            //Injects the constructors of all DI-ed objects 
            //with a LinqToSQL implementation of IRepository
            Bind<IVariableRepository>().To<VariableRepository>();
            Bind<IUserRepository>().To<UserRepository>();
        }
        
    }
}