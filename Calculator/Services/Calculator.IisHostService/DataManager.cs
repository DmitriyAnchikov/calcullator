﻿using Calculator.Common;
using Calculator.Data;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calculator.Service
{
    public class DataManager
    {
        [Inject]
        IVariableRepository Variables { get; set; }
        [Inject]
        IUserRepository Users { get; set; }

        public DataManager(IVariableRepository varRep, IUserRepository userRep)
        {
            Variables = varRep;
            Users = userRep;
        }

        public void SaveVariable(int userId, string password, Variable variable)
        {
            var existingVar = Variables.Get(userId, password, variable.Name);
            if (existingVar != null)
            {
                // todo logic for case of already existing variable with that name
                Variables.Update(userId, password, variable);
            }
            // now overwriting is realized 
            Variables.Create(userId, password, variable);
        }

        public Variable GetVariable(int userId, string password, string varName)
        {
            return Variables.Get(userId, password, varName);
        }
        public int Register(string UserName, string Password, bool isRegistration)
        {
            return Users.RegisterOrLogin(UserName, Password, isRegistration);
        }
    }
}