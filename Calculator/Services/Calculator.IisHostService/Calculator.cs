﻿using System;
using Calculator.Data;
using Ninject;
using Calculator.Common;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Threading.Tasks;

namespace Calculator.Service
{
    public class Calculator
    {
        [Inject]
        DataManager DataMngr { get; set; }

        public Calculator(IVariableRepository varRep, IUserRepository userRep)
        {
            DataMngr = new DataManager(varRep, userRep);
        }
        /// <summary>
        /// Highest abstraction level method of calculation ierarchy
        /// </summary>
        /// <param name="userId">identifier of user in service</param>
        /// <param name="password">user's secret  password</param>
        /// <param name="newStatement">users calculation statement</param>
        /// <param name="message">result service message</param>
        /// <returns>true if calculation is succes or false if it fails</returns>
        public Task<string> Calculate(int userId, string password, string newStatement)
        {
            return Task.Run(() =>
                {
                    string message;
                    Variable stmnt = null;
                    if (!Variable.TryStatementParse(newStatement, out stmnt))
                    {
                        return stmnt.Message;
                    }
                    if (stmnt.Status != StatementStatus.WaitForResolving)
                    {
                        DataMngr.SaveVariable(userId, password, stmnt);
                        return stmnt.Message;     // "Statement is put-in.";
                    }
                    TryResolveVariable(userId, password, stmnt, out message);
                    return message;
                });
        }
        bool TryAssignAwaitedVariables(int userId, string password, Variable stmnt, out string message)
        {
            message = null;
            var awaitedVariables = stmnt.Expression.GetAllNamedOperandsRecursive();
            var undefinedVariables = new List<string>();
            foreach (var awaitedVar in awaitedVariables)
            {
                string result;
                if (!TryGetVariable(userId, password, awaitedVar.Name, out result, out message))
                {
                    undefinedVariables.Add(awaitedVar.Name);
                    continue;
                }
                awaitedVar.AssignTo(result);
            }
            if (undefinedVariables.Any())
            {
                bool moreThanOne = undefinedVariables.Count > 1;
                message = String.Format("Operand{0} {1} {2} undefined.", moreThanOne ? "s" : "",
                    String.Format("'{0}'", String.Join("', '", undefinedVariables)), moreThanOne ? "are" : "is");
                DataMngr.SaveVariable(userId, password, stmnt);
                return false;
            }
            return true;
        }
        bool TryResolveVariable(int userId, string password, Variable variable, out string message)
        {
            if (!TryAssignAwaitedVariables(userId, password, variable, out message))
            {
                return true;
            }

            if (!variable.TryCalculate())
            {
                message = variable.Message;
                return false;
            }
            message = variable.Message;
            DataMngr.SaveVariable(userId, password, variable);
            return variable.Status == StatementStatus.Calculated || variable.Status == StatementStatus.WaitForResolving;
        }
        public bool TryGetVariable(int userId, string password, string varName, out string result, out string message)
        {
            var variable = DataMngr.GetVariable(userId, password, varName);
            message = null;
            result = null;
            if (variable == null)
            {
                message = String.Format("Undefined variable: '{0}'.",varName);
                return false;
            }
            if (variable.Value != null)
            {
                result = ((dynamic)variable.Value).ToString(CultureInfo.InvariantCulture);
                return true;
            }
            if (variable.TryCalculate())
            {
                DataMngr.SaveVariable(userId, password, variable);
                result = ((dynamic)variable.Value).ToString(CultureInfo.InvariantCulture);
                return true; 
            }
            if (variable.Status == StatementStatus.WaitForResolving)
            {
                if (!TryResolveVariable(userId, password, variable, out message))
                {
                    return false;
                }
                if(variable.Value == null)
                {
                    result = null ;
                    return false;
                }
                result = ((dynamic)variable.Value).ToString(CultureInfo.InvariantCulture);   
                return true;
            }
            message = variable.Message;
            return false;
        }
        public int Register(string UserName, string Password, bool isRegistration)
        {
            return DataMngr.Register(UserName, Password, isRegistration);
        }
    }
}
