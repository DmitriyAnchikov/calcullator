﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Models;
using Calculator.Common;

namespace Calculator.Data
{
    public class OperationRepository : IOperationRepository, IDisposable
    {
        CalculatorContext db = new CalculatorContext();

        #region Члены IOperationRepository

        public Operation Get(int id)
        {
            OperationDTO stmnt = null;
            stmnt = db.Operations.FirstOrDefault(s => s.Id == id);
            return Mapper.Map(stmnt);
        }

        public void Create(Operation oper)
        {
            db.Operations.Add(Mapper.Map(oper));
            db.SaveChanges();
        }

        public void Delete(Operation oper)
        {
            db.Operations.Remove(Mapper.Map(oper));
            db.SaveChanges();
        }

        public void Update(Operation oper)
        {
            var newOper = Mapper.Map(oper);
            var oldOper = db.Operations.Include(o => o.FirstVar).Include(o => o.SecondVar).FirstOrDefault(s => s.FirstVar.Name == newOper.FirstVar.Name);
            oldOper.FirstVar = newOper.FirstVar;
            oldOper.SecondVar = newOper.SecondVar;
            oldOper.Operator = newOper.Operator;
            db.Entry(oldOper).State = EntityState.Modified;
            db.SaveChanges();
        }

        #endregion

        #region Члены IDisposable

        public void Dispose()
        {
            db.Dispose();
        }

        #endregion
    }
}
