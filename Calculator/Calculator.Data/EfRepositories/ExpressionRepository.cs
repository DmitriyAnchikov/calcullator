﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Models;
using Calculator.Common;

namespace Calculator.Data
{
    public class ExpressionRepository : IExpressionRepository, IDisposable
    {
        CalculatorContext db = new CalculatorContext();

        #region Члены IExpressionRepository

        public Expression Get(int id)
        {
            ExpressionDTO exp = null;
            exp = db.Expressions.FirstOrDefault(s => s.Id == id);
            return Mapper.Map(exp);
        }

        public void Create(Expression exp)
        {
            var newExp = Mapper.Map(exp);
            db.Expressions.Add(newExp);
            db.SaveChanges();
        }

        public void Delete(Expression exp)
        {
            var newExp = Mapper.Map(exp);
            db.Expressions.Remove(newExp);
            db.SaveChanges();
        }

        public void Update(Expression exp)
        {
            var newExp = Mapper.Map(exp);
            var oldExp = db.Expressions.Include(s => s.First).Include(s => s.Second).FirstOrDefault(s => s.Id == newExp.Id);
            oldExp.First = newExp.First;
            oldExp.Second = newExp.Second;
            oldExp.Operator = newExp.Operator;
            db.Entry(oldExp).State = EntityState.Modified;
            db.SaveChanges();
        }

        #endregion

        #region Члены IDisposable

        public void Dispose()
        {
            db.Dispose();
        }

        #endregion
    }
}
