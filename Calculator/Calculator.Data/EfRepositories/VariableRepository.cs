﻿using System;
using System.Data.Entity;
using System.Linq;
using Calculator.Data.Models;
using Calculator.Common;

namespace Calculator.Data
{
    public class VariableRepository : IVariableRepository, IDisposable
    {
        CalculatorContext db = new CalculatorContext();
        
        #region Члены IVariableRepository

        public Variable Get(int userId, string password, string name)
        {
            VariableDTO var = null;
            var = db.Variables.Include(v => v.User).FirstOrDefault(s => s.Name == name && s.User.Id == userId && s.User.Password == password && s.isDeleted == false);
            if (var == null)
            {
                return null;
            }
            return Mapper.Map(var);
        }

        public Variable[] GetAll(int userId, string password, params string[] variables)
        {
            if (variables == null || !variables.Any())
            {
                var userVariables = db.Variables.Include(v => v.User).Where(s => s.User.Id == userId && s.User.Password == password && s.isDeleted == false);
                if (userVariables == null || !userVariables.Any())
                {
                    return null;
                }
                return userVariables.Select(v => Mapper.Map(v)).ToArray();
            }
            return variables.Select(var => Mapper.Map(db.Variables.Include(v => v.User)
                .FirstOrDefault(s => s.Name == var && s.User.Id == userId && s.User.Password == password)))
                .Where(v => v != null).ToArray();
        }

        public void Create(int userId, string password, Variable var)
        {
            var newVar = Mapper.Map(var);
            var user = db.Users.FirstOrDefault(u => u.Id == userId && u.Password == password);
            if (user == null)
            {
                return;
            }
            newVar.isDeleted = false;
            newVar.User = user;
            newVar.UserId = user.Id;
            db.Variables.Add(newVar);
            db.SaveChanges();
        }

        public void Delete(int userId, string password, Variable var)
        {
            Update(userId, password, var, true);
        }

        public void Update(int userId, string password, Variable var, bool onDelete = false)
        {
            var updateVar = Mapper.Map(var);
            var user = db.Users.FirstOrDefault(u => u.Id == userId && u.Password == password);
            if (user == null)
            {
                return;
            }
            updateVar.User = user;
            var oldVar = db.Variables.Include(v => v.User).FirstOrDefault(v => v.Name == updateVar.Name && v.User.Id == userId && v.User.Password == password);
            if (oldVar == null)
            {
                return;
            }
            oldVar.Name = updateVar.Name;
            oldVar.DataType = updateVar.DataType;
            oldVar.Value = updateVar.Value;
            oldVar.Expression = updateVar.Expression;
            oldVar.Scope = updateVar.Scope;
            oldVar.isDeleted = onDelete;
            db.Entry(oldVar).State = EntityState.Modified;
            db.SaveChanges();
        }

        #endregion

        #region Члены IDisposable

        public void Dispose()
        {
            db.Dispose();
        }

        #endregion
    }
}
