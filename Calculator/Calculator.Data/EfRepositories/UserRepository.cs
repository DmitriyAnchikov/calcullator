﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Models;
using System.Data.Entity;
using Calculator.Common;


namespace Calculator.Data
{
    public class UserRepository : IUserRepository, IDisposable
    {
        CalculatorContext db = new CalculatorContext();

        public UserAccount Get(int id)
        {
            UserDTO exp = null;
            exp = db.Users.FirstOrDefault(s => s.Id == id);
            return Mapper.Map(exp);
        }

        public int RegisterOrLogin(string userName, string userPassword, bool isRegistration)
        {
            var sameNamedUsers = db.Users.Where(u=>u.Name==userName);
            if (isRegistration && !sameNamedUsers.Any())
            {
                var newUser = new UserDTO() { Name = userName , Password = userPassword}; ;
                db.Users.Add(newUser);
                db.SaveChanges();
                return newUser.Id;
            }
            if (!isRegistration && sameNamedUsers.Any())
            {
                var user = sameNamedUsers.FirstOrDefault(u => u.Password == userPassword);
                return (user == null ) ? -1 : user.Id;
            }
            return -1;
        }

        public void Delete(UserAccount user)
        {
            Update(user, true);
        }

        public void Update(UserAccount user, bool onDelete = false)
        {
            var newUser = Mapper.Map(user);
            var oldUser = db.Users.FirstOrDefault(s => s.Id == newUser.Id);
            if (oldUser == null)
            {
                return;
            } 
            oldUser.Name = newUser.Name;
            oldUser.Password = user.UserPassword;
            oldUser.IsDeleted = onDelete;
            db.Entry(oldUser).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
