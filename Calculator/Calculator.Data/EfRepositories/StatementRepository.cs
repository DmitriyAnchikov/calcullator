﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Models;
using Calculator.Common;

namespace Calculator.Data
{
    public class StatementRepository : IStatementRepository, IDisposable
    {
        CalculatorContext db = new CalculatorContext();

        #region Члены IStatementRepository

        public Statement Get(string name)
        {
            StatementDTO stmnt = null;
            stmnt = db.Statements.FirstOrDefault(s => s.Variable.Name == name);
            return Mapper.Map(stmnt);
        }

        public void Create(Statement stmnt)
        {
            db.Statements.Add(Mapper.Map(stmnt));
            db.SaveChanges();
        }

        public void Delete(Statement stmnt)
        {
            db.Statements.Remove(Mapper.Map(stmnt));
            db.SaveChanges();
        }

        public void Update(Statement stmnt)
        {
            var newStmnt = Mapper.Map(stmnt);
            var oldStmnt = db.Statements.Include(s => s.Variable).Include(s => s.Expression).FirstOrDefault(s => s.Variable.Name == newStmnt.Variable.Name);
            oldStmnt.Expression = newStmnt.Expression;
            db.Entry(oldStmnt).State = EntityState.Modified;
            db.SaveChanges();
        }

        #endregion

        #region Члены IDisposable

        public void Dispose()
        {
            db.Dispose();
        }

        #endregion
    }
}
