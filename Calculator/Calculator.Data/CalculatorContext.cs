﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Data.Models;

namespace Calculator.Data
{
    public class CalculatorContext : DbContext
    {
        public CalculatorContext()
            : base("DBConnection")
        {}
 
    public DbSet<VariableDTO> Variables { get; set; }
    
    public DbSet<UserDTO> Users { get; set; }
    }
}
