﻿using Calculator.Common;

namespace Calculator.Data
{
    public interface IVariableRepository
    {
        Variable Get(int userId, string password, string name);
        Variable[] GetAll(int userId, string password, params string[] variables);
        void Create(int userId, string password, Variable var);
        void Delete(int userId, string password, Variable var);
        void Update(int userId, string password, Variable var, bool onDelete = false);
    }
    public interface IUserRepository
    {
        UserAccount Get(int id);
        int RegisterOrLogin(string userName, string password, bool isRegistration);
        void Delete(UserAccount user);
        void Update(UserAccount user, bool onDelete=false);
    }
}
