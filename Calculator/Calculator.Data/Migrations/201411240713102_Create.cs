namespace Calculator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Expressions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstId = c.Int(),
                        SecondId = c.Int(),
                        Operator = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Variables", t => t.FirstId)
                .ForeignKey("dbo.Variables", t => t.SecondId)
                .Index(t => t.FirstId)
                .Index(t => t.SecondId);
            
            CreateTable(
                "dbo.Variables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DataType = c.String(),
                        Value = c.String(),
                        Scope = c.Int(nullable: false),
                        ExpressionId = c.Int(),
                        UserId = c.Int(),
                        isDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Expressions", t => t.ExpressionId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.ExpressionId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Expressions", "SecondId", "dbo.Variables");
            DropForeignKey("dbo.Expressions", "FirstId", "dbo.Variables");
            DropForeignKey("dbo.Variables", "UserId", "dbo.Users");
            DropForeignKey("dbo.Variables", "ExpressionId", "dbo.Expressions");
            DropIndex("dbo.Variables", new[] { "UserId" });
            DropIndex("dbo.Variables", new[] { "ExpressionId" });
            DropIndex("dbo.Expressions", new[] { "SecondId" });
            DropIndex("dbo.Expressions", new[] { "FirstId" });
            DropTable("dbo.Users");
            DropTable("dbo.Variables");
            DropTable("dbo.Expressions");
        }
    }
}
