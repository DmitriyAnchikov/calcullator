namespace Calculator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExpressionDTOes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Operator = c.String(),
                        First_ID = c.Int(),
                        Second_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VariableDTOes", t => t.First_ID)
                .ForeignKey("dbo.VariableDTOes", t => t.Second_ID)
                .Index(t => t.First_ID)
                .Index(t => t.Second_ID);
            
            CreateTable(
                "dbo.VariableDTOes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DataType = c.String(),
                        Value = c.String(),
                        Scope = c.Int(nullable: false),
                        ExpressionId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        isDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ExpressionDTOes", t => t.ExpressionId, cascadeDelete: false)
                .ForeignKey("dbo.UserDTOes", t => t.UserId, cascadeDelete: false)
                .Index(t => t.ExpressionId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserDTOes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExpressionDTOes", "Second_ID", "dbo.VariableDTOes");
            DropForeignKey("dbo.ExpressionDTOes", "First_ID", "dbo.VariableDTOes");
            DropForeignKey("dbo.VariableDTOes", "UserId", "dbo.UserDTOes");
            DropForeignKey("dbo.VariableDTOes", "ExpressionId", "dbo.ExpressionDTOes");
            DropIndex("dbo.VariableDTOes", new[] { "UserId" });
            DropIndex("dbo.VariableDTOes", new[] { "ExpressionId" });
            DropIndex("dbo.ExpressionDTOes", new[] { "Second_ID" });
            DropIndex("dbo.ExpressionDTOes", new[] { "First_ID" });
            DropTable("dbo.UserDTOes");
            DropTable("dbo.VariableDTOes");
            DropTable("dbo.ExpressionDTOes");
        }
    }
}
