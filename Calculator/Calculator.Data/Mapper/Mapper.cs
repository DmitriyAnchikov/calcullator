﻿using Calculator.Common;
using Calculator.Data.Models;
using System.Globalization;

namespace Calculator.Data
{
    public static class Mapper
    {
        #region User
        public static UserAccount Map(UserDTO user)
        {
            return new UserAccount() { UserId = user.Id, UserName = user.Name , UserPassword = user.Password};
        }
        public static UserDTO Map(UserAccount user)
        {
            return new UserDTO() { Id = user.UserId, Name = user.UserName, Password = user.UserPassword };
        }
        #endregion

        
        #region Variable
        public static Variable Map(VariableDTO variable)
        {
            Expression exp =null;
            if(variable.Expression != null) 
            {
                Expression.TryParse(variable.Expression, out exp);
            }
            return new Variable((VarScope)variable.Scope, variable.Name, GetValue(variable), exp);
        }
        public static VariableDTO Map(Variable variable)
        {
            return new VariableDTO() { Name = variable.Name, Value = variable.Value == null ? null : ((dynamic)variable.Value).ToString(CultureInfo.InvariantCulture), DataType = variable.DataType == null ? null : variable.DataType.ToString(), Scope = (int)variable.Scope, Expression = variable.Expression == null ? null : variable.Expression.StringForm };
        }
        #endregion

        static object GetValue(VariableDTO variable)
        {
            switch (variable.DataType)
            {
                case "System.Double":
                    return double.Parse(variable.Value, NumberStyles.Number, CultureInfo.InvariantCulture);
                case "System.Boolean":
                    return bool.Parse(variable.Value);
            }
            return null;
        }
    }
}
