﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Data.Models
{
    public class StatementDTO
    {
        public int Id { get; set; }

        public int? VariableId { get; set; }
        public virtual VariableDTO Variable {get;set; }

        public int? ExpressionId { get; set; }
        public virtual ExpressionDTO Expression { get; set; }

        public int StatementStatus { get; set; }
        public string Message { get; set; }
    }
}
