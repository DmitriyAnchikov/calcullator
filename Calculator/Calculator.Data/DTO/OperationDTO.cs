﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Data.Models
{
    public class OperationDTO
    {
        public int Id { get; set; }
        public int FirstVarId { get; set; }
        public virtual VariableDTO FirstVar { get; set; }

        public int SecondVarId { get; set; }
        public virtual VariableDTO SecondVar { get; set; }

        public string Operator { get; set; }
    }
}
