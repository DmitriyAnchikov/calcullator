﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Data.Models
{
    [Table("Expressions")]
    public class ExpressionDTO
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        //[ForeignKey("VariableId")]
        public virtual VariableDTO First { get; set; }
        public int? FirstId { get; set; }
        //[ForeignKey("VariableId")]
        public VariableDTO Second { get; set; }
        public int? SecondId { get; set; }
        public string Operator { get; set; }
    }
}
