﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Data.Models
{
    [Table("Variables")]
    public class VariableDTO
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Name { get; set; }
        public string DataType { get; set; }
        public string Value { get; set; }
        public int Scope { get; set; }
        public string Expression { get; set; }
        public int? UserId { get; set; }
        public virtual UserDTO User { get; set; }
        public bool isDeleted { get; set; }
    }
}
